#pragma once

#include <NeuralNetLib/common.hpp>
#include <NeuralNetLib/ChiSquaredNetData.hpp>
#include "PointWithDeviation.hpp"
#include <fstream>
#include <NeuralNetLib/Utilities.hpp>

std::string getMainOutputPath(std::string exeName = "");

template<class InputType, class OutputType>
std::ostream& operator<<(std::ostream& out, PointWithDeviation<InputType, OutputType> point)
{
    return out<<point.input<<" "<<point.output<<" "<<point.deviations;
};

template<class InputType, class OutputType>
ChiSquaredNetData<InputType, OutputType>
    transpose(const std::vector<PointWithDeviation<InputType, OutputType>>& input)
{
    ChiSquaredNetData<InputType, OutputType> out{};
    for(const auto& point : input)
    {
        out.points.push_back(std::make_pair(point.input, point.output));
        out.deviations.push_back(point.deviations);
    }
    return out;
}

template<class InputType, class OutputType, class Container>
ChiSquaredNetData<InputType, OutputType>
transpose(Container&& input)
{
    ChiSquaredNetData<InputType, OutputType> out{};
    for(auto& point : input)
    {
        out.points.push_back(std::make_pair(point.input, point.output));
        out.deviations.push_back(point.deviations);
    }
    return out;
}

template<class InputType, class OutputType, class Container>
ChiSquaredNetData<InputType, OutputType>
    transposeAndAddSpecialData(Container&& input,
                               const std::vector<PointWithDeviation<InputType, OutputType>>& specialData)
{
    ChiSquaredNetData<InputType, OutputType> out{};
    for(auto& point : input)
    {
        out.points.push_back(std::make_pair(point.input, point.output));
        out.deviations.push_back(point.deviations);
    }
    for(const auto& point : specialData)
    {
        out.points.push_back(std::make_pair(point.input, point.output));
        out.deviations.push_back(point.deviations);
    }
    return out;
}


struct CorruptedDataException : public std::exception {};

template<std::size_t n>
std::istream& operator>>(std::istream& in, std::array<ArithmeticType, n>& array)
{
    for(auto& a : array)
    {
        if(not in.good())
            throw CorruptedDataException{};
        in>>a;
    }
    return in;
}

template<class InputType, class OutputType>
std::istream& operator>>(std::istream& in, PointWithDeviation<InputType, OutputType>& point)
{
    in>>point.input;
    in>>point.output;
    in>>point.deviations;
    return in;
};

std::istream& eatWhitespace(std::istream& in);

template<class T>
std::vector<T> readData(const std::string& filename)
{
    std::vector<T> out;
    std::ifstream in(filename);
    if(not in.is_open())
        std::cerr << "Unable to open file: " << filename << "\n", abort();
    std::stringstream sstream;
    sstream << in.rdbuf();
    in.close();
    while(sstream.good())
    {
        T point;
        try{
            sstream>>point;
            out.push_back(std::move(point));
            eatWhitespace(sstream);
        } catch (const CorruptedDataException& e)
        {
            std::cerr << "Data corrupted! "<<out.size()<<" data points read, rest of file discarded.\n";
        }
    }
    return out;
};

template<class Type>
void writeData(const Type& data, const std::string& filename)
{
    std::ofstream out(filename);
    out<<data;
    out.close();
}

template<class Type>
void writeData(const std::vector<Type>& data, const std::string& filename)
{
    std::ofstream out(filename);
    for(const auto& a : data)
        out<<a<<"\n";
    out.close();
}

void createFolderIfNotExisting(const std::string& folderPath);

using SingleInput = std::array<ArithmeticType, 1>;
using SingleOutput = std::array<ArithmeticType, 1>;
using oneToOneData = ChiSquaredNetData<SingleInput, SingleOutput>;

oneToOneData readOneToOneDataWithDeviations(const std::string& filename);
Range getInputsRange(std::vector<PointWithDeviation<SingleInput, SingleOutput>> data);
Range getInputsRange(std::vector<std::pair<SingleInput, SingleOutput>> data);
