#include <iomanip>
#include <sstream>
#include <iostream>
#include "EstimatedFinishTimeLogger.hpp"

template<class Clock, class Duration>
std::string to_string(const std::chrono::time_point<Clock, Duration> time)
{
    auto time_t = std::chrono::system_clock::to_time_t(time);
    std::stringstream ss;
    ss << std::put_time(std::localtime(&time_t), "%X");
    return ss.str();

}

EstimatedFinishTimeLogger::EstimatedFinishTimeLogger(unsigned n): n(n){}

void EstimatedFinishTimeLogger::log(const std::string& msg) {
    std::cout << to_string(std::chrono::system_clock::now()) << "\t" << msg << std::endl;
}

void EstimatedFinishTimeLogger::advanceAndLog(const std::string& msg) {
    auto now = std::chrono::system_clock::now();
    double scale = n / double(++count);
    auto estimation = startTime + std::chrono::duration_cast<std::chrono::nanoseconds>((now - startTime) * scale);
    std::cout << to_string(now)
              << "\tprogress: " << count << "/" << n
              << "\testimated time: " << to_string(estimation) << msg << std::endl;
}
