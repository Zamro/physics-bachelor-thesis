#pragma once
#include <chrono>
#include <string>

class EstimatedFinishTimeLogger {
    unsigned n;
    unsigned count = 0;
    std::chrono::system_clock::time_point startTime = std::chrono::system_clock::now();
public:
    explicit EstimatedFinishTimeLogger(unsigned count);
    void log(const std::string& msg);
    void advanceAndLog(const std::string& msg = "");
};


