#pragma once

template<class InputType, class OutputType>
struct PointWithDeviation{
    InputType input;
    OutputType output;
    OutputType deviations;
};
