#include <iostream>
#include <cmath>
#include <numeric>
#include <fstream>
#include <sstream>
#include <algorithm>

#include <NeuralNetLib/Neurons/Neuron.hpp>
#include <NeuralNetLib/Neurons/SigmoidalNeuron.hpp>
#include <NeuralNetLib/Neurons/LinearNeuron.hpp>
#include <NeuralNetLib/NeuralNet.hpp>
#include <NeuralNetLib/LearningAlgorithms/Backprop.hpp>
#include <NeuralNetLib/LearningAlgorithms/Quickprop.hpp>
#include <NeuralNetLib/LearningAlgorithms/WeightDecay.hpp>
#include <NeuralNetLib/Utilities.hpp>
#include "filesystemUtilities.hpp"

struct PlotData{
    std::string path;
    bool lines;
    std::string plotName;
};

void plot(
        std::vector<PlotData> input,
        std::string output = "",
        const std::string& title ="",
        const std::string& x = "epoka",
        const std::string& y = "blad",
        bool logscale = false)
{
//    std::locale::global(std::locale("PL-pl"));
    if(output == "")
        output = input[0].path.substr(0, input[0].path.size() - 4) + ".png";
    std::string scriptFilename = output.substr(0, output.size() - 4) + ".sc";
    std::ofstream out(scriptFilename);
    out<<"set key right\n"
       <<"set title \"" + title + "\"\n"
       <<"set ylabel \""+y+"\"\n"
       <<"set xlabel \""+x+"\"\n"
       <<(logscale?"set logscale y \n":"")
       <<"set terminal png "
       <<"size 800, 600"
       <<"\n"
       <<"set output \"" + output + "\"\n"
       <<"plot ";
    for(auto data : input)
        out<<" \"" + data.path+ "\" using 1:2" + (data.lines?" with lines lw 2":":3 with yerrorbars ps 0.7")
             + " title '"+ data.plotName + "',\\\n";
    out.close();
    system(("gnuplot " + scriptFilename).c_str());
    std::cerr<<"saving "<<output<<std::endl;
}

void plotCreatedFiles(const std::string &filesPrefix,
                      const std::string &title,
                      const std::string &xAxis,
                      const std::string &yAxis,
                      unsigned numberOfFiles,
                      bool logPlot = false,
                      std::vector<PlotData> &&plots = {})
{
    for(unsigned j=0; j < numberOfFiles; j++)
        plots.push_back({filesPrefix + std::to_string(j + 1) + ".dat",
                         true,
                         "siec 1-" + std::to_string(j + 1) + "-1"});
    plot(plots,
         filesPrefix + ".png",
         title,
         xAxis,
         yAxis,
         logPlot);
}

template<unsigned i>
void createFilesForBestNets(
        const LearningAlgorithm& learningAlgorithm,
        const ErrorFunction<SingleOutput>& errorFunction,
        unsigned maxEpochs,
        unsigned randomNetsTried,
        const oneToOneData& data,
        const std::string& folderPathWithParametersString)
{
    createFilesForBestNets<i-1>(learningAlgorithm, errorFunction, maxEpochs, randomNetsTried, data, folderPathWithParametersString);

    using Net = NeuralNet<SigmoidalNeuron, LinearNeuron, 1, i, 1>;
    auto bestNetWithErrors = getBestNetWithLearningErrors<Net>(data.points,
                                                               maxEpochs,
                                                               randomNetsTried,
                                                               learningAlgorithm,
                                                               errorFunction);
    auto errorFile = folderPathWithParametersString + "_error" + std::to_string(i) + ".dat";
    auto outFile = folderPathWithParametersString + "_output" + std::to_string(i) + ".dat";
    bestNetWithErrors.saveToFiles(errorFile,
                                  outFile,
                                  getInputsRange(data.points));
}

template<>
void createFilesForBestNets<0>(const LearningAlgorithm&,
                               const ErrorFunction<SingleOutput>&,
                               unsigned,
                               unsigned,
                               const oneToOneData&,
                               const std::string& )
{}

template<unsigned maxNumberOfHiddenNeurons>
void testLearning(
        const LearningAlgorithm& learningAlgorithm,
        const ErrorFunction<SingleOutput>& errorFunction,
        unsigned epochs,
        unsigned randomNetsTried,
        const oneToOneData& data)
{
    std::string folderPath = getMainOutputPath() + "/" + learningAlgorithm.getName() + "/";
    createFolderIfNotExisting(folderPath);
    std::string folderPathWithParametersString = folderPath
                                                 + learningAlgorithm.toString()
                                                 + "_epochs_"
                                                 + std::to_string(epochs);

    createFilesForBestNets<maxNumberOfHiddenNeurons>(learningAlgorithm,
                                                     errorFunction,
                                                     epochs,
                                                     randomNetsTried,
                                                     data,
                                                     folderPathWithParametersString);

    std::string titlesEnding = "przy pomocy algorytmu "
                            + learningAlgorithm.getName()
                            + " dla parametrów "
                            + learningAlgorithm.toString();
    std::replace(titlesEnding.begin(), titlesEnding.end(), '_', ' ');

    plotCreatedFiles(folderPathWithParametersString + "_output",
                     std::string("Funkcja otrzymana ") + titlesEnding,
                     "Q^2[GeV^2]",
                     "G_M(Q^2)",
                     maxNumberOfHiddenNeurons,
                     false,
                     {{"GMp_normal_data.dat", false, "dane"}});

    plotCreatedFiles(folderPathWithParametersString + "_error",
                     std::string("Błąd podczas nauki ") + titlesEnding,
                     "Epoka",
                     errorFunction.getName(),
                     maxNumberOfHiddenNeurons,
                     true);
}

int main(int argc, char* argv[])
{
    std::string fileName = argc == 1 ? "GMp_normal_data.dat" : argv[1];
    auto data = readOneToOneDataWithDeviations(fileName);
    unsigned randomNetsTried = 200;
    unsigned maxEpochsLearned = 1000;
    const unsigned maxHiddenLayer = 5;
    double lambda = 0.2;
    for(double learningRate = 0.08e-6; learningRate <= 10e-6 ; learningRate *= 2){
        if(learningRate < 2e-6) {
            testLearning<maxHiddenLayer>(Backprop{{learningRate}},
                                         ChiSquaredError<SingleOutput>(data.deviations),
                                         maxEpochsLearned,
                                         randomNetsTried,
                                         data);
            testLearning<maxHiddenLayer>(WeightDecay<Backprop>{{lambda, learningRate}},
                            WeightDecayError<ChiSquaredError<SingleOutput>>(ChiSquaredError<SingleOutput>(data.deviations), lambda),
                            maxEpochsLearned,
                            randomNetsTried,
                            data);
        }
        testLearning<maxHiddenLayer>(Quickprop{{learningRate}},
                                     ChiSquaredError<SingleOutput>(data.deviations),
                                     maxEpochsLearned,
                                     randomNetsTried,
                                     data);
        testLearning<maxHiddenLayer>(WeightDecay<Quickprop>{{lambda, learningRate}},
                        WeightDecayError<ChiSquaredError<SingleOutput>>(ChiSquaredError<SingleOutput>(data.deviations), lambda),
                        maxEpochsLearned,
                        randomNetsTried,
                        data);
    }

    return 0;
}
