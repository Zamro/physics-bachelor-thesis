#include <iomanip>
#include "MeanVariancePerParameter.hpp"

void MeanVariancePerParameter::insert(const Parameters& p, ArithmeticType error) {
    kMap[p.k].addValue(error);
    learningRateMap[p.learningRate].addValue(error);
    muMap[p.maximumGrowthFactor].addValue(error);
}

template<class T>
std::ostream& operator<<(std::ostream& out, std::map<T, MeanVarianceCalculator> map)
{
    out<<",\t      mean,\tstd. deviation\n";
    for(auto& pair : map)
        out<<std::setw(12)<<pair.first<<",\t"<<std::setw(10)<<pair.second.getMean()<<",\t"<<std::setw(14)<<pair.second.getStandardDeviation()<<"\n";
    return out;
}

void MeanVariancePerParameter::write(const std::string& filename) {
    std::ofstream out(filename);
    out<<"k:\n"           <<"           k"<<kMap<<"\n";
    out<<"learningRate:\n"<<"learningRate"<<learningRateMap<<"\n";
    out<<"mu:\n"          <<"          mu"<<muMap<<"\n";
    out.close();
}
