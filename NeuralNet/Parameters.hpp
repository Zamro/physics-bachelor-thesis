#pragma once
#include <map>
#include <cstdlib>
#include <NeuralNetLib/Utilities.hpp>

#define DEFAULT_randomNetsTried 500
#define DEFAULT_maxEpochs 5000
#define DEFAULT_k 4
#define DEFAULT_learningRate 2.56e-5
#define DEFAULT_maximumGrowthFactor 1.5
#define DEFAULT_maxEpochsValidationErrorIsNotFalling 500
#define DEFAULT_monteCarloGenerated 20
#define DEFAULT_numberOfSpecialPoints 1
#define DEFAULT_dirSuffix "KFold"
#define DEFAULT_verbose 0
#define DEFAULT_nRangeBegin 1
#define DEFAULT_nRangeEnd 5

struct Parameters{
    std::string exeFile = "";
    std::string dataFile = "";
    unsigned randomNetsTried = DEFAULT_randomNetsTried;
    unsigned maxEpochs = DEFAULT_maxEpochs;
    unsigned k = DEFAULT_k;
    double learningRate = DEFAULT_learningRate;
    double maximumGrowthFactor = DEFAULT_maximumGrowthFactor;
    unsigned maxEpochsValidationErrorIsNotFalling = DEFAULT_maxEpochsValidationErrorIsNotFalling;
    unsigned monteCarloGenerated = DEFAULT_monteCarloGenerated;
    unsigned numberOfSpecialPoints = DEFAULT_numberOfSpecialPoints;
    std::string dirSuffix = DEFAULT_dirSuffix;
    int verbose = DEFAULT_verbose;
    int nRangeBegin = DEFAULT_nRangeBegin;
    int nRangeEnd = DEFAULT_nRangeEnd;
};

std::ostream& operator<<(std::ostream& out, Parameters p);

Parameters readParameters(unsigned argc, char* argv[]);

void printHelpAndAbort(std::string exeName);

extern std::map<char, std::pair<std::string, std::function<void(char**, unsigned&, Parameters&)>>> paramMap;
