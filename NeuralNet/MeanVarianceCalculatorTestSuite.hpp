#pragma once
#include "MeanVarianceCalculator.hpp"
#include <NeuralNetLib/Tests/Utils/TestSuite.hpp>

struct MeanVarianceCalculatorTestSuite : public TestSuite{
    MeanVarianceCalculator sut;

    void parseData(const std::vector<ArithmeticType>& set)
    {
        for(auto a : set)
            sut.addValue(a);
    }

    TEST(MeanAndVarianceOfEmptySetShouldBeZero, MeanVarianceCalculatorTestSuite)
    {
        ASSERT_EQ(sut.getMean(), 0);
        ASSERT_EQ(sut.getVariance(), 0);
    }

    TEST(MeanAndVarianceOfOneElementSetShouldBeValueAndZero, MeanVarianceCalculatorTestSuite)
    {
        parseData({3.});
        ASSERT_EQ(sut.getMean(), 3.);
        ASSERT_EQ(sut.getVariance(), 0);
    }

    TEST(MeanAndVarianceOfTwoElementSet, MeanVarianceCalculatorTestSuite)
    {
        parseData({1., 3.});
        ASSERT_EQ(sut.getMean(), 2.);
        ASSERT(std::abs(sut.getVariance() - 2.0) < 0.0001);
    }

    TEST(MeanAndVarianceOfBiggerSet, MeanVarianceCalculatorTestSuite)
    {
        parseData({1., 2., 3., 4.,5. ,6.});
        ASSERT_EQ(sut.getMean(), 3.5);
        ASSERT_EQ(sut.getVariance(), 3.5);
        ASSERT(std::abs(sut.getVariance() - 3.5) < 0.0001);
    }

    TEST(MeanAndVarianceOfComplicatedSet, MeanVarianceCalculatorTestSuite)
    {
        parseData({1.0, 1.2, 1.4, 1.6, 1.8, 2.0});
        ASSERT_EQ(sut.getMean(), 1.5);
        ASSERT(std::abs(sut.getVariance() - 0.14) < 0.0001);
    }

    static void test() {
        TestSuite::test({
            &MeanAndVarianceOfEmptySetShouldBeZero,
            &MeanAndVarianceOfOneElementSetShouldBeValueAndZero,
            &MeanAndVarianceOfTwoElementSet,
            &MeanAndVarianceOfBiggerSet,
            &MeanAndVarianceOfComplicatedSet
        });
    }
};
