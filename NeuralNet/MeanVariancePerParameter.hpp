#pragma once
#include <string>
#include <NeuralNetLib/common.hpp>
#include "Parameters.hpp"
#include "MeanVarianceCalculator.hpp"

class MeanVariancePerParameter {
    std::map<unsigned, MeanVarianceCalculator> kMap;
    std::map<double, MeanVarianceCalculator> learningRateMap;
    std::map<double, MeanVarianceCalculator> muMap;
public:
    void insert(const Parameters& p, ArithmeticType error);
    void write(const std::string& filename);
};


