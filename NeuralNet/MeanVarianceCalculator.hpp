#pragma once
#include <NeuralNetLib/Utilities.hpp>

//Wolford's Online algorithm for counting variance
class MeanVarianceCalculator{
    unsigned count = 0;
    ArithmeticType mean = 0;
    ArithmeticType M2 = 0;
public:
    void addValue(ArithmeticType value);
    ArithmeticType getMean() const;
    ArithmeticType getVariance() const; //unbiased estimator
    ArithmeticType getStandardDeviation() const; //unbiased estimator
};


