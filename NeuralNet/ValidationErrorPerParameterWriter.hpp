#pragma once
#include <sstream>
#include <limits>
#include "Parameters.hpp"

class MeanVarianceCalculator;

class ValidationErrorPerParameterWriter {
    unsigned count = 0;
    Parameters bestParameters;
    double lowestValidationError = std::numeric_limits<double>::max();
    std::stringstream validationErrorsPerParameter;
public:
    ValidationErrorPerParameterWriter();
    void push_back(const MeanVarianceCalculator& validationError, const Parameters& p, const MeanVarianceCalculator& bestEpoch);
    const Parameters& getBestParameters() const;
    void write(const std::string& filename) const;
};
