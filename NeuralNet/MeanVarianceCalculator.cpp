#include "MeanVarianceCalculator.hpp"

void MeanVarianceCalculator::addValue(ArithmeticType value) {
    ++count;
    auto delta = value - mean;
    mean = mean + delta / count;
    M2 = M2 + delta * (value - mean);
}

ArithmeticType MeanVarianceCalculator::getMean() const{
    return mean;
}

ArithmeticType MeanVarianceCalculator::getVariance() const{
    return count > 1 ? M2 / (count - 1) : 0.;
}

ArithmeticType MeanVarianceCalculator::getStandardDeviation() const {
    return std::sqrt(getVariance());
}
