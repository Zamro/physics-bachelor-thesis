#include <cstdlib>
#include <NeuralNetLib/Utilities.hpp>
#include <map>
#include <NeuralNetLib/Neurons/SigmoidalNeuron.hpp>
#include <NeuralNetLib/Neurons/LinearNeuron.hpp>
#include <NeuralNetLib/NeuralNet.hpp>
#include <NeuralNetLib/LearningAlgorithms/Quickprop.hpp>
#include "filesystemUtilities.hpp"
#include "KFold.hpp"
#include "MonteCarlo.hpp"
#include "MeanNet.hpp"
#include "Parameters.hpp"
#include "EstimatedFinishTimeLogger.hpp"
#include "MonteCarloKFoldNetsWriter.hpp"

using DataWithDeviations = std::vector<PointWithDeviation<SingleInput, SingleOutput>>;

std::vector<double> getAllErrors(const std::vector<NetSaveWithStartingPoint>& randomNets)
{
    std::vector<double> errors;
    std::transform(randomNets.begin(),
                   randomNets.end(),
                   std::back_inserter(errors),
                   [](const NetSaveWithStartingPoint& net){return net.validationError;});
    return errors;
}

std::vector<unsigned> getAllEpochs(const std::vector<NetSaveWithStartingPoint>& randomNets)
{
    std::vector<unsigned> epochs;
    std::transform(randomNets.begin(),
                   randomNets.end(),
                   std::back_inserter(epochs),
                   [](const NetSaveWithStartingPoint& net){return net.startingPoint.epochs;});
    return epochs;
}

template<unsigned hiddenUnits>
void learnNetAndGenerateOutput(
        const DataWithDeviations& data,
        const DataWithDeviations& specialData,
        const Range& range,
        const Parameters& p,
        const std::string& outputPath)
{
    using Net = NeuralNet<SigmoidalNeuron, LinearNeuron, 1, hiddenUnits, 1>;
    MeanNet<Net> meanNet;
    auto monteCarlo = makeMonteCarlo(data);
    auto learningAlgorithm = Quickprop({p.learningRate, p.maximumGrowthFactor});
    bool verboseLog = (p.verbose & 1) != 0;
    bool verboseWrite = (p.verbose & 2) != 0;
    std::vector<double> errors;
    std::vector<unsigned> epochs;
    EstimatedFinishTimeLogger timeEstimator(p.monteCarloGenerated);
    if(verboseLog)
        timeEstimator.log("monte carlo net learning started for " + std::to_string(hiddenUnits) + " hidden units");
    for(unsigned i = 0; i < p.monteCarloGenerated; i++)
    {
        auto monteCarloData = monteCarlo.generate();
        auto kSets = makeKFold(monteCarloData, p.k);
        unsigned setNumber = 0;
        auto&& set = kSets[setNumber];
        auto learningData = transposeAndAddSpecialData(set.learningSet, specialData);
        auto validationData = transpose(set.getValidationSet());

        auto learningErrorFunction = ChiSquaredError<typename Net::OutputType>(learningData.deviations);
        auto validationErrorFunction = ChiSquaredError<typename Net::OutputType>(validationData.deviations);
        auto randomNets = trainRandomNets<Net>(learningData.points,
                                               p.maxEpochs,
                                               p.randomNetsTried,
                                               learningAlgorithm,
                                               learningErrorFunction,
                                               validationData.points,
                                               validationErrorFunction,
                                               p.maxEpochsValidationErrorIsNotFalling);
        auto bestNetStartingPoint = std::min(randomNets.begin(), randomNets.end());
        auto prefix = outputPath + std::to_string(hiddenUnits) + "_mc" + std::to_string(i);

        meanNet.push_back(Net{bestNetStartingPoint->save.begin()});
        writeData(getAllErrors(randomNets), prefix + "_randomNetsErrors.txt");
        writeData(getAllEpochs(randomNets), prefix + "_randomNetsEpochs.txt");
        errors.push_back(bestNetStartingPoint->validationError);
        epochs.push_back(bestNetStartingPoint->startingPoint.epochs);

        if(verboseWrite) {
            auto bestNetWithErrors = learnNetAndGetLearningErrors<Net>(bestNetStartingPoint->startingPoint,
                                                                       learningData.points,
                                                                       validationData.points,
                                                                       learningAlgorithm,
                                                                       learningErrorFunction,
                                                                       validationErrorFunction);
            saveNetWithOutputAndSets(prefix, bestNetWithErrors, range, set.learningSet, set.getValidationSet());
        }
        if(verboseLog)
            timeEstimator.advanceAndLog();
    }
    auto prefix = outputPath + std::to_string(hiddenUnits);
    meanNet.writeOutput(prefix + "_meanOutput.dat", range);
    writeData(errors, prefix + "_randomNetsErrors.txt");
    writeData(epochs, prefix + "_randomNetsEpochs.txt");
}

template <unsigned maxHiddenUnits>
void learnNetAndGenerateOutputVariable(
        unsigned hiddenUnits,
        const DataWithDeviations& data,
        const DataWithDeviations& specialData,
        const Range& range,
        const Parameters& p,
        const std::string& outputPath)
{
    if(maxHiddenUnits == hiddenUnits)
        return learnNetAndGenerateOutput<maxHiddenUnits>(data, specialData, range, p, outputPath);

    return learnNetAndGenerateOutputVariable<maxHiddenUnits - 1>(hiddenUnits, data, specialData, range, p, outputPath);
}

template<>
void learnNetAndGenerateOutputVariable<0>(
        unsigned hiddenUnits,
        const DataWithDeviations& data,
        const DataWithDeviations& specialData,
        const Range& range,
        const Parameters& p,
        const std::string& outputPath)
{
    std::cerr<<"Error - Number of hidden units outside of values permitted in compilation! : n = "<<hiddenUnits<<"\n";
}

int main(int argc, char* argv[])
{
    Parameters p = readParameters(argc, argv);

    auto outputPath = getMainOutputPath(p.dirSuffix) + "/";
    writeData(p, outputPath + "parameters.txt");

    DataWithDeviations data = readData<PointWithDeviation<SingleInput, SingleOutput>>(p.dataFile);
    Range range = getInputsRange(data);
    auto specialData = DataWithDeviations(data.begin(), data.begin() + p.numberOfSpecialPoints);
    data.erase(data.begin(), data.begin() + p.numberOfSpecialPoints);

    for(int i = p.nRangeBegin; i <= p.nRangeEnd; i++)
        learnNetAndGenerateOutputVariable<15>(i, data, specialData, range, p, outputPath);

    return 0;
}
