#pragma once

#include <vector>
#include <string>
#include "filesystemUtilities.hpp"
#include <NeuralNetLib/Utilities.hpp>
#include "MeanVarianceCalculator.hpp"

template<class Net>
class MeanNet {
public:
    void push_back(Net&& net)
    {
        nets.push_back(std::move(net));
    }

    void writeOutput(const std::string& fileName, Range range) {
        std::ofstream out(fileName);
        constexpr auto n = typename Net::OutputType{}.size();
        for(auto x = range.begin; x <= range.end; x += range.delta)
        {
            unsigned count = 0;
            std::array<MeanVarianceCalculator, n> meanVarianceCalculators;
            for(auto& net : nets)
            {
                ++count;
                auto y = net.predict({x});
                for(unsigned i = 0; i < n; i++)
                    meanVarianceCalculators[i].addValue(y[i]);
            }
            out << x << " ";
            for(unsigned i = 0; i < n; i++)
                out << meanVarianceCalculators[i].getMean()<<" ";
            for(unsigned i = 0; i < n; i++)
                out << meanVarianceCalculators[i].getVariance()<<" ";
            out << "\n";
        }
        out.close();
    }

    typename Net::OutputType predictMean(typename Net::InputType input)
    {
        typename Net::OutputType meanOutput{};
        for(auto& net : nets)
        {
            auto netOutput = net.predict(input);
            for(unsigned i = 0; i < meanOutput.size(); i++)
                meanOutput[i] += netOutput[i];
        }
        for(unsigned i = 0; i < meanOutput.size(); i++)
            meanOutput[i] /= nets.size();

        return meanOutput;
    };

    typename Net::OutputType predictMax(typename Net::InputType input)
    {
        typename Net::OutputType maxOutput{};
        for(auto& net : nets)
        {
            auto netOutput = net.predict(input);
            for(unsigned i = 0; i < maxOutput.size(); i++)
                maxOutput[i] = std::max(maxOutput[i],netOutput[i]);
        }
        return maxOutput;
    };

private:
    std::vector<Net> nets;
};
