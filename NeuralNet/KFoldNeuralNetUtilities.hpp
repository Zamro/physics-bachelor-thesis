#pragma once

#include <NeuralNetLib/LearningAlgorithms/Quickprop.hpp>
#include <NeuralNetLib/Utilities.hpp>
#include "PointWithDeviation.hpp"
#include "KFold.hpp"
#include "filesystemUtilities.hpp"

struct KFoldNetStartingPoint{
    std::vector<double> startingPoint;
    std::vector<unsigned> epochs;
    double validationErrorSum = std::numeric_limits<double>::max();
};

template<class Net>
KFoldNetStartingPoint getBestNetsKFold(
        const KFold<PointWithDeviation<typename Net::InputType, typename Net::OutputType>>& kfoldData,
        const std::vector<PointWithDeviation<typename Net::InputType, typename Net::OutputType>>& specialData,
        unsigned maxEpochs = 100,
        unsigned numberOfTries = 100,
        const LearningAlgorithm& learningAlgorithm = Backprop{},
        unsigned maxEpochsValidationErrorIsNotFalling = std::numeric_limits<unsigned>::max())
{
    KFoldNetStartingPoint bestNet{};
    for(unsigned i = 0; i < numberOfTries; i++)
    {
        Net randomNet;
        double errorsSum = 0;
        unsigned epochsSum = 0;
        std::vector<unsigned> epochs;
        for(auto&& set : kfoldData)
        {
            auto learningSet = transposeAndAddSpecialData(set.learningSet, specialData);
            auto validationSet = transpose(set.getValidationSet());
            auto learningErrorFunction = ChiSquaredError<typename Net::OutputType>(learningSet.deviations);
            auto validationErrorFunction = ChiSquaredError<typename Net::OutputType>(validationSet.deviations);
            auto net = trainNetEarlyStopping(learningSet.points,
                                             maxEpochs,
                                             numberOfTries,
                                             learningAlgorithm,
                                             learningErrorFunction,
                                             validationSet.points,
                                             validationErrorFunction,
                                             maxEpochsValidationErrorIsNotFalling,
                                             Net(randomNet.saveToString()));
            errorsSum += net.validationError;
            epochs.push_back(net.startingPoint.epochs);
            epochsSum += net.startingPoint.epochs;
        }
        if(errorsSum < bestNet.validationErrorSum)
        {
            epochs.push_back(epochsSum / kfoldData.size());
            bestNet = {randomNet.save(), std::move(epochs), std::move(errorsSum)};
        }
    }
    return bestNet;
}

template<class Net>
struct KFoldLearnedNet{
    std::vector<NetWithLearningErrors<Net, std::pair<ArithmeticType, ArithmeticType>>> kfoldNets;
    NetWithLearningErrors<Net, std::pair<ArithmeticType, ArithmeticType>> resultNet;
};

template<class Net>
KFoldLearnedNet<Net> getBestNetsWithLearningErrorsKFold(
        const KFold<PointWithDeviation<typename Net::InputType, typename Net::OutputType>>& kfoldData,
        const std::vector<PointWithDeviation<typename Net::InputType, typename Net::OutputType>>& specialData,
        const std::vector<PointWithDeviation<typename Net::InputType, typename Net::OutputType>>& allData,
        const Parameters& p)
{
    KFoldLearnedNet<Net> out;
    auto learningAlgorithm = Quickprop({p.learningRate, p.maximumGrowthFactor});
    auto startingPoints = getBestNetsKFold<Net>(kfoldData,
                                           specialData,
                                           p.maxEpochs,
                                           p.randomNetsTried,
                                           learningAlgorithm,
                                           p.maxEpochsValidationErrorIsNotFalling);

    for(unsigned i = 0; i < kfoldData.size(); i++)
    {
        auto startingPoint = StartingPoint{startingPoints.startingPoint, startingPoints.epochs[i]};
        auto learningSet = transposeAndAddSpecialData(kfoldData[i].learningSet, specialData);
        auto validationSet = transpose(kfoldData[i].getValidationSet());
        auto learningErrorFunction = ChiSquaredError<typename Net::OutputType>(learningSet.deviations);
        auto validationErrorFunction = ChiSquaredError<typename Net::OutputType>(validationSet.deviations);
        out.kfoldNets.push_back(learnNetAndGetLearningErrors<Net>(startingPoint,
                                                                  learningSet.points,
                                                                  validationSet.points,
                                                                  learningAlgorithm,
                                                                  learningErrorFunction,
                                                                  validationErrorFunction));
    }
    auto startingPoint = StartingPoint{startingPoints.startingPoint, startingPoints.epochs.back()};
    auto learningSet = transpose(allData);
    auto learningErrorFunction = ChiSquaredError<typename Net::OutputType>(learningSet.deviations);
    out.resultNet = learnNetAndGetLearningErrors<Net>(startingPoint,
                                                      learningSet.points,
                                                      learningSet.points,
                                                      learningAlgorithm,
                                                      learningErrorFunction,
                                                      learningErrorFunction);
    return out;
}
