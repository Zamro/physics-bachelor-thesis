#include "KFoldTestSuite.hpp"
#include "MeanVarianceCalculatorTestSuite.hpp"


int main()
{
    KFoldTestSuite::test();
    MeanVarianceCalculatorTestSuite::test();
}
