#pragma once
#include <vector>
#include <ostream>

std::vector<int> generateRange(unsigned length, int begin = 0);
std::vector<std::vector<unsigned>> generateKFoldPartitioning(unsigned n, unsigned k, unsigned layerWidth = 1);

template<class T>
struct KFold;

template<class T>
struct LearningSet
{
    const KFold<T>* kfold;
    unsigned i;
    unsigned size()
    {
        return kfold->dataSize - kfold->subsets[i].size();
    }

    struct constIterator : public std::iterator<std::forward_iterator_tag, T>
    {
        const KFold<T>* kfold = nullptr;
        unsigned i = 0;
        unsigned currentSet = 0;
        typename std::vector<T>::const_iterator it = {};
        constIterator(const KFold<T>* kfold, unsigned i, unsigned currentSet, typename std::vector<T>::const_iterator it):
                kfold(kfold), i(i), currentSet(currentSet), it(it){}
        bool operator==(const constIterator& b) const {
            return currentSet == b.currentSet
                   && (
                           currentSet == kfold->size()
                           || b.it == it
                   );
        }
        bool operator!=(const constIterator& b) const {return !operator==(b);}
        const T& operator*(){return *it;}
        const T* operator->(){return &(*it);}
        constIterator& operator++(){
            ++it;
            if(it == kfold->subsets[currentSet].end())
            {
                ++currentSet; if(currentSet == i) ++currentSet;
                if(currentSet < kfold->size())
                    it = kfold->subsets[currentSet].begin();
            }
            return *this;
        }
        const constIterator operator++(int){constIterator tmp = *this; operator++(); return tmp;}
        constIterator operator+(int dif) const{
            auto addedSet = currentSet;
            auto addedIt = it;
            while(dif > kfold->subsets[addedSet].end() - addedIt)
            {
                dif -= kfold->subsets[addedSet].end() - addedIt;
                ++addedSet; if(addedSet == i) ++addedSet;
                addedIt = kfold->subsets[addedSet].begin();
            }
            return constIterator(kfold, i, addedSet, addedIt);
        }
        bool operator<(const constIterator& b) const{return currentSet < b.currentSet || it < b.it;}
    };

    constIterator begin() const{
        unsigned currentSet = (i > 0 ? 0 : 1);
        return constIterator(kfold, i, currentSet, kfold->subsets[currentSet].begin());
    }
    constIterator end() const{
        return constIterator(kfold, i, kfold->size(), typename std::vector<T>::const_iterator());
    }
};

template<class T>
std::ostream& operator<<(std::ostream& out, const LearningSet<T>& set)
{
    for(auto it = set.begin(); it < set.end(); ++it)
        out << *it << "\n";
    return out;
}

template<class T>
struct KFoldSubsets
{
    const KFold<T>* kfold;
    unsigned i;
    LearningSet<T> learningSet{kfold, i};
    const std::vector<T>& getValidationSet(){return kfold->subsets[i];}
};

template<class T>
struct KFold {
    friend LearningSet<T>;
    friend KFoldSubsets<T>;
    KFold(const std::vector<T>& data, unsigned k, unsigned layerWidth = 1):k(k),subsets(k), dataSize(data.size()) {
        auto partitioning = generateKFoldPartitioning(dataSize, k, layerWidth);
        for(unsigned i = 0; i < k; i++)
            for(auto index : partitioning[i])
                subsets[i].push_back(data[index]);
    }

    unsigned size() const
    {
        return k;
    }

    struct constIterator : public std::iterator<std::random_access_iterator_tag, T, int>
    {
        const KFold<T>* kfold;
        unsigned i;
        constIterator(const KFold<T>* kfold, unsigned i):kfold(kfold), i(i){}
        constIterator(const constIterator&) = default;
        bool operator==(constIterator& b){return b.kfold == kfold && b.i == i;}
        bool operator!=(constIterator& b){return !operator==(b);}
        KFoldSubsets<T> operator*(){return {kfold, i};}
        KFoldSubsets<T> operator->(){return {kfold, i};}
        constIterator& operator++(){++i; return *this;}
        const constIterator operator++(int){constIterator tmp = *this; i++; return tmp;}
        constIterator& operator--(){--i; return *this;}
        const constIterator operator--(int){constIterator tmp = *this; i--; return tmp;}
        constIterator& operator+=(int dif){i += dif; return *this;}
        constIterator& operator-=(int dif){i -= dif; return *this;}
        constIterator operator+(int dif){return {kfold, i-dif};}
        constIterator operator-(int dif){return {kfold, i+dif};}
        int operator-(constIterator& b){return i - b.i;}
        bool operator<(constIterator& b){return i < b.i;}
        KFoldSubsets<T> operator[](unsigned n) const {return {this, i + n};}
    };

    constIterator begin() const
    {
        return {this, 0};
    }
    constIterator end() const
    {
        return {this, k};
    }
    KFoldSubsets<T> operator[](unsigned i) const
    {
        return {this, i};
    }
private:
    unsigned k;
    std::vector<std::vector<T>> subsets;
    unsigned dataSize;
};

template<class T>
KFold<T> makeKFold(const std::vector<T>& data, unsigned k, unsigned layerWidth = 1)
{
    return KFold<T>(data, k, layerWidth);
}
