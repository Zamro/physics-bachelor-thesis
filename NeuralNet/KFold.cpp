#include "KFold.hpp"
#include <random>
#include <chrono>

namespace
{
    std::default_random_engine randomEngine(std::chrono::system_clock::now().time_since_epoch().count());
}

std::vector<int> generateRange(unsigned length, int begin)
{
    std::vector<int> out;
    for(unsigned i = 0; i < length; i++)
        out.push_back(i + begin);
    return out;
}

std::vector<std::vector<unsigned>> generateKFoldPartitioning(unsigned n, unsigned k, unsigned int layerWidth) {
    std::vector<std::vector<unsigned>> partitionedIndexes(k);
    for(unsigned i = 0; i < n;)
    {
        auto availableIndexes = generateRange(std::min(k * layerWidth, n - i), i);
        while(not availableIndexes.empty())
        {
            std::uniform_int_distribution<unsigned> distribution (0, availableIndexes.size() - 1);
            auto index = distribution(randomEngine);
            auto indexIterator = availableIndexes.begin() + index;
            partitionedIndexes[i%k].push_back(*indexIterator);
            availableIndexes.erase(indexIterator);
            ++i;
        }
    }
    return partitionedIndexes;
}
