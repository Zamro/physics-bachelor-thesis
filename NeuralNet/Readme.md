# NeuralNet for approximating nucleons form-factors based on the experimental data

## Just a quick note:
For all targets use cmake. Example:

```mkdir cmake-build && cd cmake-build && cmake -DCMAKE_BUILD_TYPE:STRING=Release ..```

Further assumption is, that commands are called inside ``cmake-build`` directory
  * Targets:
    *  DataAnalysisWithMonteCarloAveraging - main application, its results are described in the ../praca/Gumny2019.pdf thesis, and it is widely used in ../praca/Mathematica/*.wl files. For usage info check help message:
      ```bash
      make DataAnalysisWithMonteCarloAveraging
      ./DataAnalysisWithMonteCarloAveraging
      ```


    *  UnitTests - launches unit tests

    Example Usage:
      ```bash
      make UnitTests
      ./UnitTests
      ```


    *  ParameterTester - performs learning for given data using a range of parameters. Takes long time.

    Usage:
      ```
      ParameterTester datafile [outputDirectorySuffix] [verbose]

      - outputDirectorySuffix [= dataFile]
      - verbose [= 0]
         bit 0 - verbose logging, bit 1 - verbose writing of neural nets
      ```
    Example Usage:
      ```bash
      make ParameterTester
      ./ParameterTester ../../materialy/dane/GEn_normal_data.dat _suffix 1
      ```


    *  LearningAlgorithmsComparison - prepare plots for comparision of learning algorithms with various leaningRate values. Needs gnuplot installed and accessible by system call. Takes long time.

    Usage:
      ```
      LearningAlgorithmsComparison [datafile = GMp_normal_data.dat]
      ```
    Example Usage:
      ```bash
      make LearningAlgorithmsComparison
      ./LearningAlgorithmsComparison ../../materialy/dane/GMp_normal_data.dat
      ```


    *  quickpropWithoutRegularization - sample application for checking quickprop algorithm and comparing its results with and without weightDecay regularization (name seems a little misleading, probably due to historical reasons). Uses linear and sinusoidal disturbed data (without randomness, for easier results comparison). Takes long time (around 25 minutes on i7 gen10 CPU), results are outputed to output/quickpropWithoutRegularizationAndStopping and output/weightDecay. It can be plotted with e.g. gnuplot.
    Example Usage:
      ```bash
      make quickpropWithoutRegularization
      ./quickpropWithoutRegularization
      ```
