#pragma once

#include "MeanNet.hpp"
#include "KFoldNeuralNetUtilities.hpp"

template <class Net, class LearningSet, class ValidationSet>
void saveNetWithOutputAndSets(const std::string& prefix,
                              Net& net,
                              const Range& range,
                              const LearningSet& learningSet,
                              const ValidationSet& validationSet)
{
    writeData(learningSet, prefix + "_learningSet.dat");
    writeData(validationSet, prefix + "_validationSet.dat");
    net.saveToFiles(prefix + "_error.dat",
                    prefix + "_output.dat",
                    range);
    net.saveNet(prefix + "_net.net");
}

template <class Net>
class MonteCarloKFoldNetsWriter {
    using Data = std::vector<PointWithDeviation<typename Net::InputType, typename Net::OutputType>>;
    MeanNet<Net> meanNet {};
    const Data& data;
    Range range;
    std::string outputPath;
    unsigned parametersIndex;
public:

    MonteCarloKFoldNetsWriter(const Data& data,
                              const Range& range,
                              const std::string& outputPath,
                              unsigned parametersIndex):
            data(data),
            range(range),
            outputPath(outputPath),
            parametersIndex(parametersIndex)
    {}

    void addAndWriteNets(KFoldLearnedNet<Net>&& nets,
                         const KFold<PointWithDeviation<typename Net::InputType, typename Net::OutputType>>& kfold,
                         unsigned monteCarloIndex)
    {
        for(unsigned setNumber = 0; setNumber < nets.kfoldNets.size(); setNumber++){
            auto& bestNetWithErrors = nets.kfoldNets[setNumber];
            auto sets = kfold[setNumber];
            auto prefix = outputPath + "pc" + std::to_string(parametersIndex)
                          + "_mc" + std::to_string(monteCarloIndex)
                          + "_set" + std::to_string(setNumber);
            saveNetWithOutputAndSets(prefix,
                                     bestNetWithErrors,
                                     range,
                                     sets.learningSet,
                                     sets.getValidationSet());
        }
        auto prefix = outputPath + "pc" + std::to_string(parametersIndex)
                      + "_mc" + std::to_string(monteCarloIndex)
                      + "_wholeData";
        saveNetWithOutputAndSets(prefix,
                                 nets.resultNet,
                                 range,
                                 data,
                                 Data{});
        meanNet.push_back(std::move(nets.resultNet.net));
    }

    void writeMeanNetOutput(){
        meanNet.writeOutput(outputPath + "pc" + std::to_string(parametersIndex) + "_meanOutput.dat", range);
    }
};
