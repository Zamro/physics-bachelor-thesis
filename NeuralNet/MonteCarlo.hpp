#pragma once

#include <utility>
#include <vector>
#include <random>
#include <chrono>
#include <NeuralNetLib/common.hpp>
#include "PointWithDeviation.hpp"

static std::default_random_engine randomEngine(std::chrono::system_clock::now().time_since_epoch().count());

template<class InputType, class OutputType>
class MonteCarlo {
    using Data = std::vector<PointWithDeviation<InputType, OutputType>>;
public:
    MonteCarlo(const Data& data) : data(data) {}

    Data generate() const
    {
        unsigned n = OutputType{}.size();
        Data copy = data;
        for(auto& point : copy)
        {
            for(unsigned i = 0; i < n; i++)
            {
                std::normal_distribution<ArithmeticType> distribution (0.0, point.deviations[i]);
                point.output[i] += distribution(randomEngine);
            }
        }
        return copy;
    }

    Data data;
};

template<class InputType, class OutputType>
MonteCarlo<InputType, OutputType> makeMonteCarlo(const std::vector<PointWithDeviation<InputType, OutputType>>& data)
{
    return MonteCarlo<InputType, OutputType>(data);
}
