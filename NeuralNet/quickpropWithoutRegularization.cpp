#include <iostream>
#include <cmath>
#include <numeric>
#include <fstream>
#include <sstream>
#include <algorithm>
#include <iomanip>

#include <NeuralNetLib/NeuralNet.hpp>
#include <NeuralNetLib/Neurons/Neuron.hpp>
#include <NeuralNetLib/Neurons/SigmoidalNeuron.hpp>
#include <NeuralNetLib/Neurons/LinearNeuron.hpp>
#include <NeuralNetLib/LearningAlgorithms/Quickprop.hpp>
#include <NeuralNetLib/LearningAlgorithms/WeightDecay.hpp>
#include <NeuralNetLib/Utilities.hpp>
#include "filesystemUtilities.hpp"

typedef std::array<ArithmeticType, 1> OutputType;


using Data = std::vector<std::pair<std::array<ArithmeticType,1>, std::array<ArithmeticType,1>>>;
Data linearData = {
        {{0.390089},  {0.391416 }},
        {{0.73001},   {0.856625 }},
        {{0.870429},  {0.894989 }},
        {{0.622415},  {0.676838 }},
        {{0.951823},  {0.875055 }},
        {{0.99823},   {1.04293 }},
        {{0.426708},  {0.315224 }},
        {{0.284138},  {0.165736 }},
        {{0.747095},  {0.748589 }},
        {{0.0189711}, {-0.0539514}}
};
Data sinData = {
        {{2.4485097568055423},  {0.8157253835267493}},
        {{2.9383063270992693},  {0.22759632278503553}},
        {{0.45515601442121056}, {0.6428093773903083}},
        {{1.60275860683182},    {1.0627913728444907}},
        {{2.5475180814739637},  {0.3693561479507588}},
        {{1.5175727961331562},  {0.886684210402799}},
        {{0.3177593668845713},  {0.2755484840131833}},
        {{3.9465399573279267},  {-0.8069981047870989}},
        {{1.6791744798351358},  {1.074801398284345}},
        {{1.2707961556148666},  {1.2617141407321113}},
        {{0.9020083217166288},  {0.8643054986564439}},
        {{5.969721726800927},   {-0.3496355082522904}},
        {{5.335191063944743},   {-0.7421083132554286}},
        {{0.7189761331217106},  {0.5927809567039006}},
        {{1.2949899991623912},  {1.0967458809817359}},
        {{3.94573401590716},    {-0.8773867680603146}},
        {{4.1636554338430685},  {-0.7915778536413799}},
        {{3.5894380099545273},  {-0.4385922413933021}},
        {{4.473244688988183},   {-1.0968717514684898}},
        {{5.099030994183286},   {-0.9900047451636133}}
};

template<unsigned node>
std::string joinNodes(char separator = '_')
{
    return std::to_string(node);
}

template<unsigned node, unsigned next, unsigned... nodes>
std::string joinNodes(char separator = '_')
{
    return std::to_string(node) + separator + joinNodes<next, nodes...>(separator);
}

template<unsigned... nodes>
void processT(const Data& data, std::string name)
{
    std::cout<<name + ": processT<" + joinNodes<nodes...>(',') + ">"<<std::endl;
    using Net = NeuralNet<SigmoidalNeuron, LinearNeuron, nodes...>;
    double learningRate = 0.075;
    auto bestNetWithErrors = getBestNetWithLearningErrors<Net>(data,
                                                               800,
                                                               200,
                                                               Quickprop{learningRate});
    createFolderIfNotExisting("output/quickpropWithoutRegularizationAndStopping");
    std::ofstream out("output/quickpropWithoutRegularizationAndStopping/" + name + "_" + std::to_string(learningRate) + "_" + joinNodes<nodes...>() + ".dat");
    for(const auto& point : generateOutput(bestNetWithErrors.net, getInputsRange(data)))
        out<<point.first[0]<<" "<<point.second[0]<<"\n";
    out.close();

}


void process(const Data& data, std::string name)
{
    processT<1,1>(data, name);
    processT<1,5,1>(data, name);
    processT<1,20,20,1>(data, name);
}

template<unsigned... nodes>
void generateWeightDecayNets(const Data& data, std::string name)
{
    using Net = NeuralNet<SigmoidalNeuron, LinearNeuron, nodes...>;
    using WDError = WeightDecayError<StandardError<typename Net::OutputType>>;
    for(double weightDecayLambda = 0; weightDecayLambda <= 2E-5; weightDecayLambda += 5E-6)
    {
        std::cout<<name + ": weightDecayNets<" + joinNodes<nodes...>(',') + ">, weightDecayLambda: " + std::to_string(weightDecayLambda)<<std::endl;
        double learningRate = 0.075;
        auto bestNetWithErrors = getBestNetWithLearningErrors<Net>(data,
                                                                   800,
                                                                   1000,
                                                                   WeightDecay<Quickprop>({weightDecayLambda, {learningRate}}),
                                                                   WDError({}, weightDecayLambda));


        createFolderIfNotExisting("output/weightDecay");
        std::ofstream out("output/weightDecay/" + name + "_" + std::to_string(weightDecayLambda) + "_" + joinNodes<nodes...>() + ".dat");
        for(const auto& point : generateOutput(bestNetWithErrors.net, getInputsRange(data)))
            out<<point.first[0]<<" "<<point.second[0]<<"\n";
        out.close();
    }
}

int main()
{
    process(linearData, "linear");
    process(sinData, "sin");
    generateWeightDecayNets<1, 5, 1>(sinData, "sin");
    generateWeightDecayNets<1, 20, 20, 1>(sinData, "sin");
    return 0;
}
