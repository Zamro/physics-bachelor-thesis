#pragma once
#include <vector>
#include "KFold.hpp"
#include <NeuralNetLib/Tests/Utils/TestSuite.hpp>
#include <algorithm>

struct TestKFoldSets{
    std::vector<int> learningSet;
    std::vector<int> validationSet;
};

struct KFoldTestSuite : public TestSuite{
    unsigned n = 10;
    unsigned k = 4;

    TEST(ShouldDivideIntoKSubSets, KFoldTestSuite)
    {
        auto subsets = generateKFoldPartitioning(n, k);
        ASSERT_EQ(subsets.size(), k);
    }

    TEST(SubSetsShouldContainRightAmountOfElements, KFoldTestSuite)
    {
        auto subsets = generateKFoldPartitioning(n, k);

        unsigned numberOfElementsInSubsets = 0;
        for(auto& set : subsets)
            numberOfElementsInSubsets += set.size();
        ASSERT_EQ(numberOfElementsInSubsets, n);
    }

    TEST(SubSetsShouldContainAllData, KFoldTestSuite)
    {
        auto subsets = generateKFoldPartitioning(n, k);
        auto data = generateRange(n);
        for(auto& set : subsets)
            for(auto d : set)
            {
                auto it = std::find(data.begin(), data.end(), d);
                if(it != data.end())
                    data.erase(it);
            }
        ASSERT(data.empty());
    }

    TEST(GivenDivisibleSetSubSetsShouldHaveEqualSize, KFoldTestSuite)
    {
        unsigned n = 12;
        unsigned subsetSize = 3;
        auto subsets = generateKFoldPartitioning(n, k);

        for(auto& set : subsets)
            ASSERT_EQ(set.size(), subsetSize);
    }

    TEST(GivenNotDivisibleSetSubSetsShouldHaveProperSizes, KFoldTestSuite)
    {
        unsigned n = 12;
        unsigned k = 5;
        std::vector<unsigned> subsetSizes = {3, 3, 2, 2, 2};
        auto subsets = generateKFoldPartitioning(n, k);

        ASSERT_EQ(subsets.size(), subsetSizes.size());
        for(unsigned i = 0; i < subsets.size(); i++)
            ASSERT_EQ(subsets[i].size(), subsetSizes[i]);
    }

    TEST(DataShouldBeEquallyDistributed, KFoldTestSuite)
    {
        auto data = generateRange(n);
        auto subsets = generateKFoldPartitioning(n, k);
        for(unsigned i = 0; i < n/k; i++)
        {
            auto subdata = std::vector<int>(data.begin() + i * k, data.begin() + (i+1)*k);
            for(const auto& subset : subsets)
            {
                auto it = std::find(subdata.begin(), subdata.end(), subset[i]);
                if(it != subdata.end())
                    subdata.erase(it);
            }
            ASSERT(subdata.empty());
        }
    }

    std::vector<std::vector<int>> generateSimpleSubsets(unsigned n, unsigned k)
    {
        std::vector<std::vector<int>> subsets(k);
        for(unsigned i = 0; i < n; i++)
            subsets[i%k].push_back(i);
        return subsets;
    }

    //This test has a probability of failing equal to (1/k!)^(n/k)*1/(n%k)! = 1/1152 = 0.09%
    TEST(DataShouldBeRandom, KFoldTestSuite)
    {
        auto subsets = generateKFoldPartitioning(n, k);
        auto simpleSubsets = generateSimpleSubsets(n, k);
        bool allSubsetsAreSame = true;
        ASSERT_EQ(subsets.size(), simpleSubsets.size());
        for(unsigned i = 0; i < subsets.size(); i++)
        {
            ASSERT_EQ(subsets[i].size(), simpleSubsets[i].size());
            for(auto point : subsets[i])
            {
                auto it = std::find(simpleSubsets[i].begin(), simpleSubsets[i].end(), point);
                if(it != simpleSubsets[i].end())
                    simpleSubsets[i].erase(it);
            }
            allSubsetsAreSame &= simpleSubsets[i].empty();
        }
        ASSERT(not allSubsetsAreSame);
    }

    //This test has a probability of failing equal to ((k!)^2 / (2k)!)^2 = 331776 / 1625702400 = 0.02 %
    TEST(GivenLayerWidthGreaterThanOneDataShouldNotBeEquallyDistributedInSmallerLayer, KFoldTestSuite)
    {
        unsigned n = 20;
        auto data = generateRange(n);
        unsigned layerWidth = 2;
        auto subsets = generateKFoldPartitioning(n, k, layerWidth);
        bool allSmallerLayersEquallyDistributed = true;
        for(unsigned i = 0; i < n/k; i++)
        {
            auto subdata = std::vector<int>(data.begin() + i * k, data.begin() + (i+1)*k);
            for(const auto& subset : subsets)
            {
                auto it = std::find(subdata.begin(), subdata.end(), subset[i]);
                if(it != subdata.end())
                    subdata.erase(it);
            }
            allSmallerLayersEquallyDistributed &= subdata.empty();
        }
        ASSERT(not allSmallerLayersEquallyDistributed);
    }

    TEST(GivenLayerWidthGreaterThanOneDataShouldBeEquallyDistributedInGivenLayer, KFoldTestSuite)
    {
        unsigned n = 20;
        auto data = generateRange(n);
        unsigned layerWidth = 2;
        auto subsets = generateKFoldPartitioning(n, k, layerWidth);
        for(unsigned i = 0; i < n/(k*layerWidth);  i++)
        {
            auto subdata = std::vector<int>(data.begin() + i * k * layerWidth, data.begin() + (i+1)*k*layerWidth);
            for(const auto& subset : subsets)
                for(unsigned l = 0; l < layerWidth; l++)
                {
                    auto it = std::find(subdata.begin(), subdata.end(), subset[layerWidth * i + l]);
                    if(it != subdata.end())
                        subdata.erase(it);
                }
            ASSERT(subdata.empty());
        }
    }

    void assertKFoldSetsAreEqual(const std::vector<TestKFoldSets>& A,
                                 const std::vector<TestKFoldSets>& B)
    {
        ASSERT_EQ(A.size(), B.size());
        for(unsigned i = 0; i < A.size(); i++)
        {
            ASSERT_EQ(A[i].learningSet.size(), B[i].learningSet.size());
            for(unsigned j = 0; j < A[i].learningSet.size(); j++)
                ASSERT_EQ(A[i].learningSet[j], B[i].learningSet[j]);
            ASSERT_EQ(A[i].validationSet.size(), B[i].validationSet.size());
            for(unsigned j = 0; j < A[i].validationSet.size(); j++)
                ASSERT_EQ(A[i].validationSet[j], B[i].validationSet[j]);
        }
    }

    void checkSimpleKFoldSets(KFold<int>& kfold) const {
        std::vector<TestKFoldSets> properSets{
                {{1, 2, 3}, {0}},
                {{0, 2, 3}, {1}},
                {{0, 1, 3}, {2}},
                {{0, 1, 2}, {3}}
        };
        for(auto&& set : kfold)
        {
            ASSERT_EQ(set.getValidationSet().size(), 1u);
            const auto& properSet = properSets[set.getValidationSet()[0]];
            ASSERT_EQ(properSet.validationSet[0], set.getValidationSet()[0]);
            for(auto val : set.learningSet)
            {
                auto it = std::find(properSet.learningSet.begin(),
                                    properSet.learningSet.end(),
                                    val);
                ASSERT(it != properSet.learningSet.end());
            }
        }
    }

    TEST(KFoldProducesKFoldSets, KFoldTestSuite)
    {
        n = 4;
        std::vector<int> data = generateRange(n);
        KFold<int> kFold(data, k);
        checkSimpleKFoldSets(kFold);
    };

    TEST(MakeKFoldProducesKFoldSets, KFoldTestSuite)
    {
        n = 4;
        std::vector<int> data = generateRange(n);
        auto kFold = makeKFold(data, k);
        checkSimpleKFoldSets(kFold);
    };

    static void test() {
        TestSuite::test({
            &ShouldDivideIntoKSubSets,
            &SubSetsShouldContainRightAmountOfElements,
            &SubSetsShouldContainAllData,
            &GivenDivisibleSetSubSetsShouldHaveEqualSize,
            &GivenNotDivisibleSetSubSetsShouldHaveProperSizes,
            &DataShouldBeEquallyDistributed,
            &DataShouldBeRandom,
            &GivenLayerWidthGreaterThanOneDataShouldNotBeEquallyDistributedInSmallerLayer,
            &GivenLayerWidthGreaterThanOneDataShouldBeEquallyDistributedInGivenLayer,
            &KFoldProducesKFoldSets,
            &MakeKFoldProducesKFoldSets
        });
    }
};
