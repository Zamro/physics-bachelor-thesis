#include <cstdlib>
#include <NeuralNetLib/Utilities.hpp>
#include <map>
#include <set>
#include <NeuralNetLib/Neurons/SigmoidalNeuron.hpp>
#include <NeuralNetLib/Neurons/LinearNeuron.hpp>
#include <NeuralNetLib/NeuralNet.hpp>
#include "filesystemUtilities.hpp"
#include "KFold.hpp"
#include "MonteCarlo.hpp"
#include "MeanNet.hpp"
#include "Parameters.hpp"
#include "MeanVariancePerParameter.hpp"
#include "KFoldNeuralNetUtilities.hpp"
#include "ValidationErrorPerParameterWriter.hpp"
#include "MonteCarloKFoldNetsWriter.hpp"
#include "EstimatedFinishTimeLogger.hpp"

using DataWithDeviations = std::vector<PointWithDeviation<SingleInput, SingleOutput>>;
using MonteCarloKFold = std::vector<KFold<PointWithDeviation<SingleInput, SingleOutput>>>;

struct VariableParameters
{
    double learningRate;
    double maximumGrowthFactor;
    unsigned k;

    Parameters join(Parameters p) const
    {
        p.learningRate = learningRate;
        p.maximumGrowthFactor = maximumGrowthFactor;
        p.k = k;
        return p;
    }
};

//generate the sets only once for each p.k to use them for each parameters combination
//it makes comparison of results more reliable, as the difference between different
//sets can be great, when mc is small
//on the other hand, comparision between different k's is not so reliable,
//as they are generated independently on small monte carlo set
//(so some of them can have "easier" divided sets, and some "harder")
std::map<unsigned, MonteCarloKFold> generateMonteCarloKFoldPerK(const DataWithDeviations& data,
                                                                const std::vector<VariableParameters> variableParametersList,
                                                                unsigned numberOfMonteCarloGeneratedSets)
{
    std::set<unsigned> ks;
    for(const auto& p : variableParametersList)
        ks.insert({p.k});

    auto monteCarlo = makeMonteCarlo(data);
    std::vector<DataWithDeviations> monteCarloData;
    for(unsigned i = 0; i < numberOfMonteCarloGeneratedSets; i++)
        monteCarloData.push_back(monteCarlo.generate());

    std::map<unsigned, MonteCarloKFold> monteCarloKFoldPerK;
    for(auto k : ks) {
        monteCarloKFoldPerK[k] = {};
        for(const auto& generatedData : monteCarloData) {
            monteCarloKFoldPerK[k].push_back(makeKFold(generatedData, k));
        }
    }
    return monteCarloKFoldPerK;
}

template <unsigned hiddenUnits>
void testParameters(const DataWithDeviations& data,
                    const DataWithDeviations& specialData,
                    const Range& range,
                    const Parameters& parameters,
                    const std::vector<VariableParameters>& variableParametersList,
                    const std::string& outputPath)
{
    using Net = NeuralNet<SigmoidalNeuron, LinearNeuron, 1, hiddenUnits, 1>;
    ValidationErrorPerParameterWriter validationErrorsPerParameter;
    MeanVariancePerParameter errorParameterMap;
    MeanVariancePerParameter epochParameterMap;
    bool verboseLog = (parameters.verbose & 1) != 0;
    bool verboseWrite = (parameters.verbose & 2) != 0;

    auto dataSize = data.size() + specialData.size();

    std::map<unsigned, MonteCarloKFold> monteCarloKFoldPerK = generateMonteCarloKFoldPerK(data,
                                                                                          variableParametersList,
                                                                                          parameters.monteCarloGenerated);

    EstimatedFinishTimeLogger timeEstimator(variableParametersList.size());
    if(verboseLog)
        timeEstimator.log("parameter testing started for " + std::to_string(hiddenUnits) + " hidden units");
    for(unsigned parameterIndex = 0; parameterIndex < variableParametersList.size(); ++parameterIndex)
    {
        Parameters p = variableParametersList[parameterIndex].join(parameters);
        MonteCarloKFoldNetsWriter<Net> netsWriter(data, range, outputPath + std::to_string(hiddenUnits) + "_", parameterIndex);
        MeanVarianceCalculator bestEpochMeanVariance;
        MeanVarianceCalculator validationErrorMeanVariance;
        for(unsigned mc = 0; mc < parameters.monteCarloGenerated; mc++){
            double validationErrorOverKFold = 0;
            double bestEpochOverKfold = 0;
            auto& kfold = monteCarloKFoldPerK[p.k][mc];
            auto bestNetsWithErrors = getBestNetsWithLearningErrorsKFold<Net>(kfold,
                                                                              specialData,
                                                                              data,
                                                                              p);
            for(unsigned setNumber = 0; setNumber < bestNetsWithErrors.kfoldNets.size(); setNumber++){
                auto& errors = bestNetsWithErrors.kfoldNets[setNumber].errors;
                auto& validationSet = kfold[setNumber].getValidationSet();
                auto bestEpoch = errors.size() - 100;

                validationErrorOverKFold += validationSet.size() * errors[bestEpoch - 1].second;
                bestEpochOverKfold += bestEpoch ;
            }
            bestEpochMeanVariance.addValue(bestEpochOverKfold / dataSize);
            epochParameterMap.insert(p, bestEpochOverKfold / dataSize);
            validationErrorMeanVariance.addValue(validationErrorOverKFold / dataSize);
            errorParameterMap.insert(p, validationErrorOverKFold / dataSize);
            if(verboseWrite)
                netsWriter.addAndWriteNets(std::move(bestNetsWithErrors), kfold, mc);
        }

        validationErrorsPerParameter.push_back(validationErrorMeanVariance, p, bestEpochMeanVariance);
        if(verboseLog)
            timeEstimator.advanceAndLog();
        if(verboseWrite)
            netsWriter.writeMeanNetOutput();
    }

    auto prefix = outputPath + "_" + std::to_string(hiddenUnits);
    writeData(validationErrorsPerParameter.getBestParameters(), prefix + "_bestParameters.txt");
    validationErrorsPerParameter.write(prefix + "_validationErrorsPerParameters.txt");
    errorParameterMap.write(prefix + "_errorParameterMap.txt");
    epochParameterMap.write(prefix + "_epochParameterMap.txt");
}

int main(int argc, char* argv[])
{
    if(argc == 1)
    {
        std::cout<<"Usage:\n  ParameterTester datafile [outputDirectorySuffix = dataFile] [verbose (bit 0 - verbose logging, bit 1 - verbose writing of neural nets)]"<<std::endl;
        return 1;
    }
    Parameters p;
    p.exeFile = argv[0];
    p.dataFile = argv[1];
    p.dirSuffix = std::string("ParameterTester") + (argc > 2 ? argv[2] : p.dataFile);
    p.randomNetsTried = 10;
    p.maxEpochs = 2000;
    p.maxEpochsValidationErrorIsNotFalling = 400;
    p.monteCarloGenerated = 10;
    p.verbose = (argc > 3 ? atoi(argv[3]) : 0);
    auto outputPath = getMainOutputPath(p.dirSuffix) + "/";

    std::vector<VariableParameters> variableParametersList{};
    for(unsigned k = 3; k <= 7; k+=2)//3 values
    for(double learningRate = 1e-12; learningRate <= 10 ; learningRate *= 4) //22 values
        for(double a = 0.35; a < 1; ((a < 0.65)?a += 0.15: a+= 0.05) ) //9 values
        {
            double maximumGrowthFactor = a / (1 - a);
            variableParametersList.push_back({learningRate, maximumGrowthFactor, k});
        }

    DataWithDeviations data = readData<PointWithDeviation<SingleInput, SingleOutput>>(p.dataFile);
    Range range = getInputsRange(data);
    auto specialData = DataWithDeviations(data.begin(), data.begin() + 1);
    data.erase(data.begin(), data.begin() + 1);

    testParameters<1>(data, specialData, range, p, variableParametersList, outputPath);
    testParameters<2>(data, specialData, range, p, variableParametersList, outputPath);
    testParameters<3>(data, specialData, range, p, variableParametersList, outputPath);
    testParameters<4>(data, specialData, range, p, variableParametersList, outputPath);
    testParameters<5>(data, specialData, range, p, variableParametersList, outputPath);

    return 0;
}
