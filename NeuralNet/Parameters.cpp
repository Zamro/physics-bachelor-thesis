#include "Parameters.hpp"

Parameters readParameters(unsigned argc, char* argv[]) {
    Parameters p;
    p.exeFile = argc ? argv[0]:"<exe name>";
    for(unsigned i = 1; i < argc; ++i)
    {
        if(argv[i][0] != '-') {
            p.dataFile = argv[i];
        }
        else {
            if(not paramMap.count(argv[i][1]) || argv[i][2] != '\0') {
                std::cerr << "Error - improper parameter: " << argv[i] << "\n";
                printHelpAndAbort(p.exeFile);
            }
            auto pair = paramMap[argv[i][1]];
            auto func = pair.second;
            func(argv, i, p);
        }
    }
    if(p.dataFile == ""){
        std::cerr<<"Error - Data file not specified!\n";
        printHelpAndAbort(p.exeFile);
    }

    return p;
}

std::ostream& operator<<(std::ostream& out, Parameters p)
{
    out<<"exeFile = "<<p.exeFile<<"\n";
    out<<"dataFile = "<<p.dataFile<<"\n";
    out<<"dirSuffix = "<<p.dirSuffix<<"\n";
    out<<"randomNetsTried = "<<p.randomNetsTried<<"\n";
    out<<"maxEpochs = "<<p.maxEpochs<<"\n";
    out<<"k = "<<p.k<<"\n";
    out<<"learningRate = "<<p.learningRate<<"\n";
    out<<"maximumGrowthFactor = "<<p.maximumGrowthFactor<<"\n";
    out<<"maxEpochsValidationErrorIsNotFalling = "<<p.maxEpochsValidationErrorIsNotFalling<<"\n";
    out<<"monteCarloGenerated = "<<p.monteCarloGenerated<<"\n";
    out<<"numberOfSpecialPoints = "<<p.numberOfSpecialPoints<<"\n";
    out<<"verbose = "<<p.verbose<<"\n";
    out<<"nRange = {"<<p.nRangeBegin<<", "<<p.nRangeEnd<<"}\n";
    return out;
}

void printHelpAndAbort(std::string exeName)
{
    std::cout << "Usage : \n" << exeName << " <dataFile> [options]" << std::endl;
    std::cout << "Possible options: \n";
    for(auto& a : paramMap)
        std::cout << "-" << a.first << " " << a.second.first << std::endl;
    abort();
}

std::map<char, std::pair<std::string, std::function<void(char**, unsigned&, Parameters&)>>> paramMap = {
        {'r', {"random nets tried - number of random nets for each set generated from monte carlo and kfold, defaults to " + std::to_string(DEFAULT_randomNetsTried),
                      [](char** args, unsigned& id, Parameters& p){p.randomNetsTried = atol(args[++id]);}}},
        {'m', {"maxEpochs - maximum number of epochs after which learning is stopped, defaults to " + std::to_string(DEFAULT_maxEpochs),
                      [](char** args, unsigned& id, Parameters& p){p.maxEpochs = atol(args[++id]);}}},
        {'k', {"k - number of sets on which data is splitted in kfold, defaults to " + std::to_string(DEFAULT_k),
                      [](char** args, unsigned& id, Parameters& p){p.k = atol(args[++id]);}}},
        {'l', {"learningRate - quickprop parameter, defaults to " + to_string(DEFAULT_learningRate),
                      [](char** args, unsigned& id, Parameters& p){p.learningRate = atof(args[++id]);}}},
        {'u', {"maximumGrowthFactor - quickprop parameter, defaults to " + to_string(DEFAULT_maximumGrowthFactor),
                      [](char** args, unsigned& id, Parameters& p){p.maximumGrowthFactor = atof(args[++id]);}}},
        {'E', {"maxEpochsValidationErrorIsNotFalling, defaults to " + std::to_string(DEFAULT_maxEpochsValidationErrorIsNotFalling),
                      [](char** args, unsigned& id, Parameters& p){p.maxEpochsValidationErrorIsNotFalling = atol(args[++id]);}}},
        {'c', {"monteCarloGenerated, defaults to " + std::to_string(DEFAULT_monteCarloGenerated),
                      [](char** args, unsigned& id, Parameters& p){p.monteCarloGenerated = atol(args[++id]);}}},
        {'n', {"numberOfSpecialPoints, defaults to " + std::to_string(DEFAULT_numberOfSpecialPoints),
                      [](char** args, unsigned& id, Parameters& p){p.numberOfSpecialPoints = atol(args[++id]);}}},
        {'d', {"directory suffix, defaults to \"" DEFAULT_dirSuffix "\"",
                      [](char** args, unsigned& id, Parameters& p){p.dirSuffix = args[++id];}}},
        {'h', {"displays this message and ends execution",
                      [](char** args, unsigned& id, Parameters& p){printHelpAndAbort(p.exeFile);}}},
        {'v', {"verbose, sets verbose logging (verbose & 1) == 1 and/or verbose file creation (verbose & 2) == 1",
                      [](char** args, unsigned& id, Parameters& p){p.verbose = atol(args[++id]);}}},
        {'R', {"rangeBegin rangeEnd - set range of hidden neurons to process, inclusive, defaults to {rangeBegin, rangeEnd} = {1, 5}",
                      [](char** args, unsigned& id, Parameters& p){p.nRangeBegin = atol(args[++id]); p.nRangeEnd = atol(args[++id]);}}}
};
