#include <fstream>
#include <iostream>
#include <algorithm>
#include <ctime>
#include <iomanip>
#include "filesystemUtilities.hpp"

void createFolderIfNotExisting(const std::string &folderPath) {
#if defined(__unix__)
    system(("if [ ! -d " + folderPath + " ] ; then  mkdir -p " + folderPath + "; fi").c_str());
#elif defined (_WIN32) || defined (_WIN64)
    auto copy = folderPath;
    std::replace(copy.begin(), copy.end(), '/','\\');
    system(("if not exist  " + copy + "  mkdir  " + copy).c_str());
#endif
}

oneToOneData readOneToOneDataWithDeviations(const std::string &filename) {
    oneToOneData out{{},{}};
    std::ifstream in(filename);
    if(not in.is_open())
        std::cerr << "Unable to open file: " << filename << "\n", abort();
    double a,b,c;
    while(in.good())
    {
        in>>a>>b>>c;
        out.points.push_back({{a},{b}});
        out.deviations.push_back({c});
    }
    in.close();
    return out;
}

std::istream &eatWhitespace(std::istream &in) {
    char a;
    if(not in.good())
        return in;
    do{
        in.get(a);
    }while(isspace(a) && in.good());
    in.unget();
    return in;
}

std::string getMainOutputPath(std::string exeName) {
    static std::string mainOutputPath = "";
    if(mainOutputPath == "")
    {
        std::time_t t = std::time(nullptr);
        std::ostringstream oss;
        oss << std::put_time(std::localtime(&t), "%y-%m-%d-%H-%M");
        mainOutputPath = "output/" + oss.str() + exeName ;
        createFolderIfNotExisting(mainOutputPath);
    }
    return mainOutputPath;
}

Range getInputsRange(std::vector<PointWithDeviation<SingleInput, SingleOutput>> data) {
    Range range{std::numeric_limits<ArithmeticType>::max(), std::numeric_limits<ArithmeticType>::min(), 0};
    for(const auto& point : data)
    {
        if(point.input[0] < range.begin)
            range.begin = point.input[0];
        else if(point.input[0] > range.end)
            range.end = point.input[0];
    }
    range.delta = (range.end - range.begin) / 200.;
    return range;
}

Range getInputsRange(std::vector<std::pair<SingleInput, SingleOutput>> data) {
    Range range{std::numeric_limits<ArithmeticType>::max(), std::numeric_limits<ArithmeticType>::min(), 0};
    for(const auto& point : data)
    {
        if(point.first[0] < range.begin)
            range.begin = point.first[0];
        else if(point.first[0] > range.end)
            range.end = point.first[0];
    }
    range.delta = (range.end - range.begin) / 200.;
    return range;
}
