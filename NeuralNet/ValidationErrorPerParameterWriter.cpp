#include "ValidationErrorPerParameterWriter.hpp"
#include <iomanip>
#include "filesystemUtilities.hpp"
#include "MeanVarianceCalculator.hpp"

ValidationErrorPerParameterWriter::ValidationErrorPerParameterWriter() {
    validationErrorsPerParameter << "mean error, del error,   pc,  k,  mu, learning rate, mean epoch, del epoch\n";
}

void ValidationErrorPerParameterWriter::push_back(const MeanVarianceCalculator& validationError,
                                                  const Parameters& p,
                                                  const MeanVarianceCalculator& bestEpoch) {
    auto precision = validationErrorsPerParameter.precision();
    validationErrorsPerParameter << std::setw(10) << validationError.getMean()
                                 << ", " << std::setw(9) << bestEpoch.getStandardDeviation()
                                 << ", " << std::setw(4) << count++
                                 << ", " << std::setw(2) << p.k
                                 << ", " << std::setw(3) << std::fixed << std::setprecision(1) << p.maximumGrowthFactor
                                 << ", " << std::setw(13) << std::defaultfloat << std::setprecision(precision) << p.learningRate
                                 << ", " << std::setw(10) << bestEpoch.getMean()
                                 << ", " << std::setw(9) << bestEpoch.getStandardDeviation()
                                 << "\n";
    if(validationError.getMean() < lowestValidationError)
    {
        lowestValidationError = validationError.getMean();
        bestParameters = p;
    }
}

void ValidationErrorPerParameterWriter::write(const std::string& filename) const {
    writeData(validationErrorsPerParameter.str(), filename);
}

const Parameters& ValidationErrorPerParameterWriter::getBestParameters() const {
    return bestParameters;
}
