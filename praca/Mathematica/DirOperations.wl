(* ::Package:: *)

BeginPackage[ "DirOperations`"]

outputDir = NotebookDirectory[]<>"..\\..\\ideaBuild\\output\\";
dataDir= NotebookDirectory[]<>"..\\..\\ideaBuild\\"
dirs[wildcard_] := FileNames[outputDir<>wildcard]
dir[pattern_, type_] := dirs["*"<>ToString[type]<>"_"<>pattern][[1]];
types = {Mp,Ep,Mn,En};
sortedDirs[pattern_] := dir[pattern, #]& /@ types;
typeFromDir[dir_] := Select[types, StringContainsQ[dir, "G"<>ToString[#]]&][[1]]

EndPackage[]
