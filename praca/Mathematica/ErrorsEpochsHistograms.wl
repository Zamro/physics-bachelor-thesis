(* ::Package:: *)

BeginPackage["ErrorsEpochsHistograms`", {"PlotGrid`", "DirOperations`"}]

exportHistogram::usage = "exportHistogram[pattern_, func_, bspec_, plotRange_, ns_:Range[1, 5]]"
exportAllHistograms::usage = "exportAllHistograms[pattern_, ns_:Range[1, 5]]"
exportUsefullHistograms;
allEpochs;
allErrors;
bestEpochs;
bestErrors;
firstEpochs;
firstErrors;

Begin["Private`"]
allEpochs[dir_, n_] := Join@@(ReadList[#]&/@(FileNames[dir<>"\\"<>ToString[n]<>"_mc*randomNetsEpochs.txt"]));
allErrors[dir_, n_] := Join@@(ReadList[#]&/@(FileNames[dir<>"\\"<>ToString[n]<>"_mc*randomNetsErrors.txt"]));
bestEpochs[dir_, n_] := ReadList[FileNames[dir<>"\\"<>ToString[n]<>"_randomNetsEpochs.txt"][[1]]];
bestErrors[dir_, n_] := ReadList[FileNames[dir<>"\\"<>ToString[n]<>"_randomNetsErrors.txt"][[1]]];
firstEpochs[dir_, n_] := ReadList[FileNames[dir<>"\\"<>ToString[n]<>"_mc0_randomNetsEpochs.txt"][[1]]];
firstErrors[dir_, n_] := ReadList[FileNames[dir<>"\\"<>ToString[n]<>"_mc0_randomNetsErrors.txt"][[1]]];

locallyStyledPlot[plots_, labels_]:=
	PlotGrid[
		plots,
		140,
		105,
		ImagePadding -> {{20, 15}, {15, 20}}, 
		Frame -> True,
		FrameTicks -> None,
		FrameStyle -> Directive[Opacity[0], FontOpacity -> 1, FontSize -> 11],
		FrameLabel -> labels
	]

myHist[data_, bspec_, opts:OptionsPattern[Histogram]]:=
	Histogram[data, bspec, PlotRangeClipping -> True, GridLines -> Automatic, Frame -> True, PlotTheme->"Monochrome", Evaluate@Apply[Sequence, {opts}]]

axesLabel[allEpochs] := "Liczba epok podczas nauki wszystkich sieci";
axesLabel[allErrors] := "B\[LSlash]\:0105d osi\:0105gni\:0119ty na ko\:0144cu nauki dla wszystkich sieci";
axesLabel[bestEpochs] := "Liczba epok podczas nauki c wybranych sieci";
axesLabel[bestErrors] := "B\[LSlash]\:0105d osi\:0105gni\:0119ty na ko\:0144cu nauki c wybranych sieci";
axesLabel[firstEpochs] := "Liczba epok podczas nauki r sieci dla jednego zbioru Monte-Carlo";
axesLabel[firstErrors] := "B\[LSlash]\:0105d osi\:0105gni\:0119ty na ko\:0144cu nauki r sieci dla jednego zbioru Monte-Carlo";

exportHistogram[pattern_, func_, bspec_, plotRange_, ns_:Range[1, 5]] :=
	(*Export[NotebookDirectory[]<>"Histograms\\"<>pattern<>"_histogram_"<>ToString[func]<>".eps",*)
	Export[NotebookDirectory[]<>ToString[func]<>"Histogram"<>".pdf",
		locallyStyledPlot[
			Table[
				Table[
					myHist[func[dir[pattern, type], n], bspec, PlotRange -> plotRange,
					FrameLabel -> {{"", "dane G"<>ToString[type]}, {"", "n = "<>ToString[n]}},
					FrameStyle->{FontSize->11}],
					{n, ns}
				],
				{type, types}
			],
			{axesLabel[func], "Liczba sieci"
		}
	]];

exportAllHistograms[pattern_, maxs_:{5000, 20, 100}, ns_:Range[1,5]] := {
	exportHistogram[pattern, allEpochs,  {50},  {{0, 2000}, {0, maxs[[1]]}}, ns],
	exportHistogram[pattern, allErrors,  {0.2}, {{0, 8},   {0, maxs[[1]]}}, ns],
	exportHistogram[pattern, bestEpochs, {50},   {{0, 2000}, {0, maxs[[2]]}}, ns],
	exportHistogram[pattern, bestErrors, {0.2}, {{0, 8},   {0, maxs[[2]]}}, ns],
	exportHistogram[pattern, firstEpochs, {50},   {{0, 2000}, {0, maxs[[3]]}}, ns],
	exportHistogram[pattern, firstErrors, {0.2}, {{0, 8},   {0, maxs[[3]]}}, ns]
}

exportUsefullHistograms[pattern_, maxs_:{20, 100}, ns_:Range[1,5]] := {
(*	exportHistogram[pattern, bestEpochs, {50},   {{0, 2000}, {0, maxs[[1]]}}, ns],*)
	exportHistogram[pattern, bestErrors, {0.2}, {{0, 8},   {0, maxs[[1]]}}, ns],
(*	exportHistogram[pattern, firstEpochs, {50},   {{0, 2000}, {0, maxs[[2]]}}, ns],*)
	exportHistogram[pattern, firstErrors, {0.2}, {{0, 8},   {0, maxs[[2]]}}, ns]
}

End[]
EndPackage[]
