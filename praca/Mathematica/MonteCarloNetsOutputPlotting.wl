(* ::Package:: *)

SetDirectory[NotebookDirectory[]];
BeginPackage[ "MonteCarloNetsOutputPlotting`", {"ErrorBarPlots`", "BayesianNeuralNets`", "DirOperations`"}]

meanPlotWithBayesianNet[dir_, n_] := Module[{
		type = typeFromDir[dir],
		output = Import[dir<>"//"<>ToString[n]<>"_meanOutput.dat"],
		data = Import[dataDir<>"G"<>ToString[typeFromDir[dir]]<>"_normal_data.dat"]},
	Show[
		ErrorListPlot[
			{{#[[1]], #[[2]]}, ErrorBar[#[[3]]]}& /@ data,
			PlotStyle -> Directive[ColorData[97, "ColorList"][[3]], Opacity -> 0.5, LineOpacity -> 0.5],
			AxesStyle -> 11
		],
		Plot[{
				netOutput[w[type], x],
				netOutput[w[type], x] - netOutputDeviation[type, x],
				netOutput[w[type], x] + netOutputDeviation[type, x]
			},
			{x, myRange[type][[1]], myRange[type][[2]]},
			Filling->{1->{3}, 1->{2}},
			PlotStyle->{Red, None, None},
			PlotRange -> myPlotRange[type]
		],
		ListLinePlot[
			Transpose[{#[[{1, 2}]], {#[[1]], #[[2]] - Sqrt[ #[[3]]]}, {#[[1]], #[[2]] + Sqrt[#[[3]]]}}&/@output],
			Filling -> {1 -> {3}, 1 -> {2}},
			PlotStyle -> {Normal, None, None}
		],
		PlotRange -> {myRange[type], myPlotRange[type]},
		GridLines -> gridLines[type],
		ImageSize -> {143, 88},
		AxesOrigin -> {0, myPlotRange[type][[1]]}
	]
]

errorMark[directives_:{}] := Graphics[Join[directives,{EdgeForm[{Opacity[0]}], Line[{{0,0},{0,1}}], Line[{{-0.1,0},{0.1,0}}], Line[{{-0.1,1},{0.1,1}}],Disk[{0,0.5},0.2]}]]
meanPlotWithBayesianNetLegend[nString_:"n"] :=
	LineLegend[
		{
			White,
			Directive[RGBColor[0.368417, 0.506779, 0.709798], AbsoluteThickness[1.6]],
			Directive[Opacity[1.], AbsoluteThickness[1.6], Red]
		},
		{
			Style["Dane wraz z niepewno\:015bciami", 11],
			Style["\:015arednia z sieci o "<>nString<>" neuronach ukrytych nauczonych w procesie Monte-Carlo wraz z niepewno\:015bci\:0105", 11],
			Style["Sie\[CAcute] z pracy \"Neural network parametrizations of electromagnetic nucleon form-factors\"", 11]
		},
		LegendMarkers->{{errorMark[{ColorData[97, "ColorList"][[3]], Opacity[0.5]}],18},errorMark[{Opacity[0]}],errorMark[{Opacity[0]}]},
		Spacings -> 0.5
		];

(*Horizontal*)
(*exportMeanPlotWithBayesianNet[pattern_, ns_:Range[1,5]] := Export[
	(*"DataAnalysis\\"<>pattern<>"_allOutputs.png",*)
	"outputAll.pdf",
	Legended[
		TableForm[
			Table[meanPlotWithBayesianNet[#, n]& /@ sortedDirs[pattern], {n, ns}],
			TableHeadings -> {Table[Rotate[Style["n = " <> ToString[n], 11], \[Pi]/2], {n, ns}], Style["Dane G"<>ToString[#], 11]&/@types},
			TableAlignments -> Center,
			TableSpacing\[Rule]{0, 0.5}
		],
		Placed[Rotate[meanPlotWithBayesianNetLegend[]], \[Pi]/2], Right]
	]
]*)
(* Vertical *)
exportMeanPlotWithBayesianNet[pattern_, ns_:Range[1,5]] := Export[
	(*"DataAnalysis\\"<>pattern<>"_allOutputs_Vert.png",*)
	"outputAll.pdf",
	Legended[
		TableForm[
			Table[meanPlotWithBayesianNet[#, n]&/@sortedDirs[pattern], {n, ns}],
			TableHeadings -> {Table["n = " <> ToString[n], {n, ns}], Rotate["Dane G"<>ToString[#], \[Pi]/2]&/@types},
			TableAlignments -> Center,
			TableDirections -> Row,
			TableSpacing -> {0, 0.5}
		],
		Placed[meanPlotWithBayesianNetLegend[], Below]
	]
]

(*exportSeparateValuesAndErrorsPlots[pattern_, ns_:Range[1,5]] := 
	Table[
		Export[
			"output"<>ToString[typeFromDir[dir]]<>".pdf",
			(*"DataAnalysis\\"<>pattern<>ToString[typeFromDir[dir]]<>"Output.png",*)
			plotsWithBayesianNetPair[dir, ns]
		],
		{dir, sortedDirs[pattern]}
	]*)
exportSeparateValuesAndErrorsPlots[pattern_, ns_:Range[1,5]] := 
	Table[
		{
			Export[
				"output"<>ToString[typeFromDir[dir]]<>".pdf",
				valuesPlotWithBayesianNet[dir, ns]
			],
			Export[
				"errors"<>ToString[typeFromDir[dir]]<>".pdf",
				errorsPlotWithBayesianNet[dir, ns]
			]
		},
		{dir, sortedDirs[pattern]}
	]

exportCombinedValuesAndErrorsPlots[pattern_, ns_:Range[1,5]] := Module[{type},Table[Table[
		{
			type  = typeFromDir[dir],
			Export[
				"output"<>ToString[n]<>"_"<>ToString[type]<>".pdf",
				Legended[
					Show[
						meanPlotWithBayesianNet[dir, n],
						ImageMargins->0,
						AxesLabel -> {"\!\(\*SuperscriptBox[\(Q\), \(2\)]\)[GeV]", realFunc[typeFromDir[dir]]},
						PlotRange -> {myRange[type], myPlotRange[type]},
						GridLines -> gridLines[type],
						ImageSize -> {625, 370},
						ImageMargins -> 0 ,
						ImagePadding->{{15, Automatic},{10, Automatic}}
					],
					Placed[meanPlotWithBayesianNetLegend[ToString[n]], Below]
				]
			]
		},
		{dir, sortedDirs[pattern]}
	],
	{n, ns}
]]


valuesPlotWithBayesianNet::usage = "valuesPlotWithBayesianNet[dir, ns:Range[1,5]] generates a plot of outputs from nets with n from ns together with bayesian net output and data of type deduced by dir.";
errorsPlotWithBayesianNet::usage = "valuesPlotWithBayesianNet[dir, ns:Range[1,5]] generates a plot of errors from nets with n from ns together with bayesian net error of type deduced by dir.";
plotsWithBayesianNetPair::usage = "plotsWithBayesianNetPair[di], ns_:Range[1,5] generates both outputs and errors plots with single legend";

Begin["Private`"]

realFunc[Mp]:="\!\(\*FractionBox[SubscriptBox[\(G\), \(Mp\)], \(\*SubscriptBox[\(\[Mu]\), \(p\)] \*SubscriptBox[\(G\), \(D\)]\)]\)(\!\(\*SuperscriptBox[\(Q\), \(2\)]\))";
realFunc[Ep]:="\!\(\*FractionBox[SubscriptBox[\(G\), \(Ep\)], SubscriptBox[\(G\), \(D\)]]\)(\!\(\*SuperscriptBox[\(Q\), \(2\)]\))";
realFunc[Mn]:="\!\(\*FractionBox[SubscriptBox[\(G\), \(Mn\)], \(\*SubscriptBox[\(\[Mu]\), \(n\)] \*SubscriptBox[\(G\), \(D\)]\)]\)(\!\(\*SuperscriptBox[\(Q\), \(2\)]\))";
realFunc[En]:="\!\(\*SubscriptBox[\(G\), \(En\)]\)(\!\(\*SuperscriptBox[\(Q\), \(2\)]\))";

mixedlegend = LineLegend[
	Join[{White, Red},ColorData[97,"ColorList"][[Range[1,5]]]],
	Style[#, 11]&/@Join[{"Dane", "Sie\[CAcute] Bayesowska"}, "n = "<>ToString[#]&/@Range[1,5]],
	LegendMarkers->{{errorMark[{ColorData[97, "ColorList"][[3]], Opacity[0.5]}],17},None,None,None, None, None, None}, Spacings -> 0.5]
errorlegend = LineLegend[
	Join[{Red},ColorData[97,"ColorList"][[Range[1,5]]]],
	Style[#, 11]&/@Join[{"Sie\[CAcute] Bayesowska"}, "n = "<>ToString[#]&/@Range[1,5]], Spacings -> 0.5]

valuesPlotWithBayesianNet[dir_, ns_:Range[1,5]] := Module[{
		type = typeFromDir[dir],
		values = Table[#[[{1, 2}]]& /@ Import[dir<>"//"<>ToString[n]<>"_meanOutput.dat"],{n,ns}],
		data = Import[dataDir<>"G"<>ToString[typeFromDir[dir]]<>"_normal_data.dat"]},
	Legended[Show[
		ErrorListPlot[
			{{#[[1]], #[[2]]}, ErrorBar[#[[3]]]}&/@data,
			PlotStyle -> Directive[ColorData[97, "ColorList"][[3]], Opacity -> 0.5, LineOpacity -> 0.5],
			AxesLabel -> {"\!\(\*SuperscriptBox[\(Q\), \(2\)]\)[GeV]", realFunc[type]}
		],
		Plot[
			netOutput[w[type], x],
			{x, myRange[type][[1]], myRange[type][[2]]},
			PlotRange -> myPlotRange[type],
			PlotStyle -> Red
		],
		ListLinePlot[
			values
		],
		PlotRange -> {myRange[type], myPlotRange[type]},
		GridLines -> gridLines[type],
		ImageSize -> {625, 340},
		ImageMargins -> 0 ,
		ImagePadding->{{15, Automatic},{10, Automatic}},
		AxesOrigin -> {0, myPlotRange[type][[1]]}
	],  Placed[mixedlegend,{{1,0.5},{0.3,0.5}}]]
]

errorRange[Mp] := {0, 0.2};
errorRange[Ep] := {0, 0.15};
errorRange[Mn] := {0, 0.15};
errorRange[En] := {0, 0.02};

errorsPlotWithBayesianNet[dir_, ns_:Range[1,5]] := Module[{
		type = typeFromDir[dir],
		values = Table[{#[[1]], Sqrt[#[[3]]]}& /@ Import[dir<>"//"<>ToString[n]<>"_meanOutput.dat"],{n,ns}],
		data = Import[dataDir<>"G"<>ToString[typeFromDir[dir]]<>"_normal_data.dat"]},
	Legended[Show[
		Plot[
			netOutputDeviation[type, x],
			{x, myRange[type][[1]], myRange[type][[2]]},
			PlotRange -> errorRange[type],
			PlotStyle -> Red,
			AxesLabel -> {"\!\(\*SuperscriptBox[\(Q\), \(2\)]\)[GeV]", "\[CapitalDelta]"<>realFunc[type]}
		],
		ListLinePlot[
			values
		],
		ImageSize -> {625, 340},
		ImageMargins -> 0 ,
		ImagePadding->{{15, Automatic},{10, Automatic}},
		GridLines -> Automatic
	], Placed[errorlegend,{{1,0.5},{0.3,0.5}}]]
]

(*plotsWithBayesianNetPair[dir_,  ns_:Range[1,5]]:=Module[
	{
		legend,
		valuesPlot = valuesPlotWithBayesianNet[dir, ns],
		errorsPlot = errorsPlotWithBayesianNet[dir, ns]
	},
	legend = Function[valuesPlot/.Graphics[__]->#];
	legend[
		TableForm[
			{
				Extract[valuesPlot,Position[valuesPlot,Graphics[__]]],
				Extract[errorsPlot,Position[errorsPlot,Graphics[__]]]
			},
			TableDirections->Row
		]
	]
];
*)

End[]
EndPackage[]
