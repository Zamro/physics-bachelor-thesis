(* ::Package:: *)

SetDirectory[NotebookDirectory[]];
BeginPackage[ "DataContourPlotsVsParameters`",{"PlotGrid`","DirOperations`"}]
getKs
getPoints
getErrors
getEpochs
readAndSplitByK
legend
myContourPlot
myScaledContourPlot
multiplotVsMuAndLearningRate
exportMultiplotsVsMuAndLearningRate
exportMultiplotsErrorVsMuAndLearningRate
exportMultiplotsEpochVsMuAndLearningRate
meanVsMuAndLearningRatePlot
errorMinMaxDif={2,15,0.5};
epochMinMaxDif={0,100,10};
Begin["Private`"]


(* ::Section:: *)
(*Importowanie i operacje na danych*)


(* ::Text:: *)
(*w zaimportowanych danych mamy: {"mean error", "del error", "pc", "k", "mu", "learning rate", "mean epoch", "del epoch"}*)
(*Poniewa\:017c wyszukanie plik\[OAcute]w jest bardzo wolne, warto\:015bci u\:017cytych n podaj\:0119 jako argumenty, zamiast wyszukiwa\[CAcute].*)
(*Timing[tab = Table[Import[dir <> "\\_" <> ToString[i] <> "_validationErrorsPerParameters.txt", "Data"], {i, 1, 5}];]*)
(*Timing[tab = Table[Import[f, "Data"], {f, FileNames[RegularExpression["_\\d+_validationErrorsPerParameters.txt"], {dir}]}];]*)


(* ::Input::Initialization:: *)
splitByK[data_]:=GatherBy[data,{#[[4]]&}];
getKs[splitted_]:=#[[1,4]]&/@splitted;
getErrors[list_]:=#[[1]]&/@list;
getEpochs[list_]:=#[[7]]&/@list;
getPoints[list_]:={#[[5]]/(#[[5]]+1),#[[6]]}&/@list
readAndSplitByK[dir_, ns_]:=Module[{datas},
	datas = Table[Import[dir<>"\\_"<>ToString[i]<>"_validationErrorsPerParameters.txt", "Data"],{i,ns}];splitByK[#[[2;;]]]&/@datas
];


(* ::Section:: *)
(*Raster contour plot*)


(* ::Text:: *)
(*https://pages.uoregon.edu/noeckel/computernotes/Mathematica/rasterContourPlot.html - u\:017cyte w celu wyeliminowania linii na wykresie, kt\[OAcute]re pojawia\[LSlash]y si\:0119 przy konkretnych czytnikach pdf (np drukowanie z Adobe Acrobat Readera, niewidoczne na Firefoxie).*)


rasterListContourPlot[pList_, opts : OptionsPattern[]] := Module[
  {img,
   cont,
   contL,
   plotRangeRule,
   contourOptions,
   frameOptions,
   rangeCoords
   },
  contourOptions = Join[
    FilterRules[{opts},
     FilterRules[Options[ListContourPlot],
      Except[{Background, Frame, Axes}]]
     ],
    {Frame -> None, Axes -> None}
    ];
  contL = ListContourPlot[pList,
    Evaluate@Apply[Sequence, contourOptions]
    ];
  cont = First[Cases[{contL}, Graphics[__], Infinity]];
  img = Rasterize[
    Graphics[
     GraphicsComplex[cont[[1, 1]], cont[[1, 2, 1]]],
     PlotRangePadding -> None, AspectRatio -> 1, ImagePadding -> None,
     Options[cont, PlotRange]
     ], "Image",
    ImageSize -> With[{size =
        Total[{2, 0} (ImageSize /. {opts}) /. {ImageSize ->
            CurrentValue[ImageSize]}]},
      If[NumericQ[size],
       size,
       First[WindowSize /. Options[EvaluationNotebook[]]]
       ]
      ]
    ];
  plotRangeRule = AbsoluteOptions[cont, PlotRange];
  rangeCoords = Transpose[PlotRange /. plotRangeRule];
  frameOptions = Join[
    FilterRules[{opts},
     FilterRules[Options[Graphics],
      Except[{PlotRangeClipping, PlotRange}]]
     ],
    {plotRangeRule, Frame -> True, PlotRangeClipping -> True}
    ];
  If[Head[contL] === Legended, Legended[#, contL[[2]]], #] &@
  Show[
   Graphics[
    {
     Inset[Show[
       SetAlphaChannel[img,
        "ShadingOpacity" /. {opts} /. {"ShadingOpacity" -> 1}],
       AspectRatio -> Full],
      rangeCoords[[1]], {0, 0}, rangeCoords[[2]] - rangeCoords[[1]]
      ]
     },
    PlotRangePadding -> None
    ],
   Graphics[
    GraphicsComplex[cont[[1, 1]], cont[[1, 2, 2]]]
    ],
   AspectRatio -> 1,
   Evaluate@Apply[Sequence, frameOptions]
   ]
  ]


(* ::Section:: *)
(*Rysowanie i eksportowanie wykres\[OAcute]w*)


(* ::Input::Initialization:: *)
colorParams[min_,max_,dif_]:= ColorData[{"GrayTones",{max,min}}];
probingPointsStyle = Gray;
(*plotMarkers = Automatic;*)
plotMarkers ={Graphics[{Gray,Disk[]}],0.015};
(*
	colorParams[min_,max_,dif_]:= ColorData[{"TemperatureMap",{min,max}}];
	probingPointsStyle = Red;
*)
cross = Graphics[{Line[{{-1,-1},{1,1}}], Line[{{-1,1},{1,-1}}]}];
legend [min_,max_,dif_]:=BarLegend[{colorParams[min,max,dif],{min,max}},Floor[(max-min)/dif - 1], LabelStyle->11];
frameLabel[n_,k_]:={{"",Style["n = "<>ToString[n],11]},{"",Style["k = "<>ToString[k],11]}};

myContourPlot[points_,values_, minMaxDif_] := Show[
	ListPlot[points,
		ScalingFunctions->{None, "Log"},
		PlotRange->Full,
		PlotStyle->White,
		PlotMarkers->None,
		ImageSize->{468,450},
		ImageMargins->0,
		AspectRatio->1,
		AxesOrigin->{0.6,10^-5},
		Axes->False,
		Frame->True,
		AxesStyle->11,
		FrameStyle->11,
		PlotRangePadding->Scaled[.02]
	],
	rasterListContourPlot[MapThread[Append,{points,values}],
		PlotRange->Full,
		ScalingFunctions->{None, "Log", None},
		ColorFunctionScaling->False,
		ColorFunction->colorParams@@minMaxDif,
		ImageSize->{468,450},
		Contours->Function[{min,max},Range[Floor[min],minMaxDif[[2]],minMaxDif[[3]]]]
	],
	ListPlot[points,
		ScalingFunctions->{None, "Log"},
		PlotRange->Full,
		PlotStyle->probingPointsStyle,
		PlotMarkers->plotMarkers
	]
];

multiplotVsMuAndLearningRate[dir_, ns_, valueGetter_, minMaxDif_]:=Module[{splitted, ks},
splitted = readAndSplitByK[dir, ns];
ks=getKs[splitted[[1]]];
Table[Table[
    Show[rasterListContourPlot[getPoints[splitted[[y,x]]],valueGetter[splitted[[y,x]]], minMaxDif],FrameLabel->frameLabel[ns[[y]],ks[[x]]]],
{x,1,Length[splitted[[y]]]}],{y,1,Length[splitted]}
]
];

exportMultiplotsVsMuAndLearningRate[directoriesWildcard_, ns_, suffix_, valueGetter_, minMaxDif_]:=
	Export[
		(*NotebookDirectory []<>"\\kFoldImages\\"<> FileBaseName[#]<>suffix,*)
		NotebookDirectory []<>"\\G"<> ToString[typeFromDir[#]]<>suffix,
		Legended[
			(*TableForm[multiplotVsMuAndLearningRate[#,ns, valueGetter, minMaxDif]],*)
			PlotGrid[
				multiplotVsMuAndLearningRate[#,ns, valueGetter, minMaxDif], 
				150,150,
				imageMargin->2,
				ImagePadding->{{30, 25}, {15,17}},
				Frame->True,
				FrameTicks->None,
				FrameStyle->Directive[Opacity[0],FontOpacity->1, FontSize->11],
				AxesStyle->11,
				FrameLabel->(Style[#,11]&/@{"\[Lambda]","\[Mu]"})],
		legend@@minMaxDif],
	ImageSize->{450,Automatic}
	]&/@dirs[directoriesWildcard];

exportMultiplotsErrorVsMuAndLearningRate[directoriesWildcard_, ns_]:=
exportMultiplotsVsMuAndLearningRate[directoriesWildcard, ns, "_errorVsParameters.pdf", getErrors, errorMinMaxDif];

exportMultiplotsEpochVsMuAndLearningRate[directoriesWildcard_, ns_]:=
exportMultiplotsVsMuAndLearningRate[directoriesWildcard, ns, "_1_epochVsParameters.png", getEpochs, epochMinMaxDif];

myScaledContourPlot[points_,values_, minMaxDif_]:=
Show[
ListContourPlot[MapThread[Append,{points,values}],
PlotRange->Full,
ScalingFunctions->{None, "Log", {Log[#-1]+2&, E^(#-2)+1&}},
 ColorFunctionScaling->False,
ColorFunction->colorParams@@{2,5,0.1},(*
ColorFunction\[Rule]ColorData[colorParams@@minMaxDif],*)
(*Contours\[Rule]Function[{min,max},Range[1,6,0.2]],
PlotLegends\[Rule]BarLegend[colorParams[1,6,0.2],Range[1,6,0.2]]*)
Contours->Function[{min,max},Range[Floor[min],5,0.1]]
],
ListPlot[points, ScalingFunctions->{None, "Log"}, PlotStyle->probingPointsStyle, PlotMarkers->plotMarkers]];

meanVsMuAndLearningRatePlot[dirs_, ns_, valueGetter_, minMaxDif_]:=Module[{splitted,split, ks},
	split = readAndSplitByK[dirs[[1]], ns];
	ks=getKs[split[[1]]];
	Legended[
Show[myContourPlot[
		getPoints[split[[1,1]]],
		Mean[Table[
			splitted = readAndSplitByK[dir, ns];
			Mean[Table[
				Mean[Table[
					valueGetter[splitted[[y,x]]],
					{x,1,Length[splitted[[y]]]}
				]],
				{y,1,Length[splitted]}
			]],
			{dir,dirs}
		]],
		minMaxDif
	],ImageSize->{468,468}, FrameLabel->(Style[#,11]&/@{"a =\!\(\*FractionBox[\(\(\\\ \)\(\[Mu]\)\), \(\[Mu] + 1\)]\)","\[Epsilon]"})], legend@@minMaxDif]
];
End[]
EndPackage[]
