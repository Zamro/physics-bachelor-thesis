(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 11.3' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[     12844,        347]
NotebookOptionsPosition[     10801,        309]
NotebookOutlinePosition[     11172,        325]
CellTagsIndexPosition[     11129,        322]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{
Cell[BoxData[{
 RowBox[{
  RowBox[{"SetDirectory", "[", 
   RowBox[{"NotebookDirectory", "[", "]"}], "]"}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{"Get", "[", "\"\<PlotGrid`\>\"", "]"}], "\[IndentingNewLine]", 
 RowBox[{"Get", "[", "\"\<DataContourPlotsVsParameters`\>\"", "]"}]}], "Input",
 InitializationCell->
  True,ExpressionUUID->"125816c6-4fcd-4623-b536-a59814d4ed04"],

Cell[CellGroupData[{

Cell["Generowanie wykres\[OAcute]w", "Section",ExpressionUUID->"551cbcb1-acd1-4b06-80b9-66d3e783a7ee"],

Cell[BoxData[
 RowBox[{"(*", 
  RowBox[{
   RowBox[{"exportMultiplotsErrorVsMuAndLearningRate", "[", 
    RowBox[{"\"\<*_MultiStructure*\\\\\>\"", ",", " ", 
     RowBox[{"Range", "[", 
      RowBox[{"1", ",", "5"}], "]"}]}], "]"}], ";"}], "*)"}]], "Input",Express\
ionUUID->"2190ceb4-9240-44f3-9460-db43e6b507dd"],

Cell[BoxData[
 RowBox[{"(*", 
  RowBox[{
   RowBox[{
    RowBox[{"exportMultiplotsErrorVsMuAndLearningRate", "[", 
     RowBox[{"\"\<*_smallerEpsilon\\\\\>\"", ",", " ", 
      RowBox[{"Range", "[", 
       RowBox[{"1", ",", "5"}], "]"}]}], "]"}], ";"}], "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"exportMultiplotsEpochVsMuAndLearningRate", "[", 
     RowBox[{"\"\<*_smallerEpsilon\\\\\>\"", ",", " ", 
      RowBox[{"Range", "[", 
       RowBox[{"1", ",", "5"}], "]"}]}], "]"}], ";"}]}], "*)"}]], "Input",Expr\
essionUUID->"e5601bcb-13f0-4ba8-b0a7-190408d386bc"],

Cell[BoxData[
 RowBox[{
  RowBox[{"exportMultiplotsErrorVsMuAndLearningRate", "[", 
   RowBox[{"\"\<*_joined\\\\\>\"", ",", " ", 
    RowBox[{"Range", "[", 
     RowBox[{"1", ",", "5"}], "]"}]}], "]"}], ";"}]], "Input",ExpressionUUID->\
"ddfc6f92-bc2b-4c77-8e52-b7c2c962de63"],

Cell[BoxData[
 RowBox[{"(*", 
  RowBox[{
   RowBox[{"exportMultiplotsEpochVsMuAndLearningRate", "[", 
    RowBox[{"\"\<*_joined\\\\\>\"", ",", " ", 
     RowBox[{"Range", "[", 
      RowBox[{"1", ",", "5"}], "]"}]}], "]"}], ";"}], "*)"}]], "Input",Express\
ionUUID->"16ec9326-c9d8-4025-ae14-37cd5e595506"],

Cell[BoxData[
 RowBox[{"(*", 
  RowBox[{
   RowBox[{"Legended", "[", "\[IndentingNewLine]", 
    RowBox[{
     RowBox[{"PlotGrid", "[", 
      RowBox[{
       RowBox[{"multiplotVsMuAndLearningRate", "[", 
        RowBox[{
         RowBox[{
          RowBox[{
          "dirs", "[", 
           "\"\<18-10-17-16-48ParameterTester_GMp_smallerEpsilon\>\"", "]"}], 
          "[", 
          RowBox[{"[", "1", "]"}], "]"}], ",", 
         RowBox[{"{", 
          RowBox[{"1", ",", "2", ",", "3", ",", " ", "4", ",", " ", "5"}], 
          "}"}], ",", " ", "getErrors", ",", " ", "errorMinMaxDif"}], "]"}], 
       ",", " ", "300", ",", "300", ",", 
       RowBox[{"ImagePadding", "\[Rule]", 
        RowBox[{"{", 
         RowBox[{
          RowBox[{"{", 
           RowBox[{"45", ",", "5"}], "}"}], ",", 
          RowBox[{"{", 
           RowBox[{"15", ",", "25"}], "}"}]}], "}"}]}], " ", ",", 
       RowBox[{"Frame", "\[Rule]", "True"}], ",", 
       RowBox[{"FrameTicks", "\[Rule]", "None"}], ",", 
       RowBox[{"FrameStyle", "\[Rule]", 
        RowBox[{"Directive", "[", 
         RowBox[{
          RowBox[{"Opacity", "[", "0", "]"}], ",", 
          RowBox[{"FontOpacity", "\[Rule]", "1"}], ",", " ", 
          RowBox[{"FontSize", "\[Rule]", "14"}]}], "]"}]}], ",", 
       RowBox[{"FrameLabel", "\[Rule]", 
        RowBox[{"{", 
         RowBox[{"\"\<\[Lambda]\>\"", ",", "\"\<\[Mu]\>\""}], "}"}]}]}], 
      "]"}], ",", "\[IndentingNewLine]", 
     RowBox[{"legend", "@@", "errorMinMaxDif"}]}], "]"}], ";"}], 
  "*)"}]], "Input",ExpressionUUID->"e260cdb1-ba55-47db-b62d-bd07fad55aa2"]
}, Open  ]],

Cell[CellGroupData[{

Cell["Pr\[OAcute]by skalowania kontur\[OAcute]w", "Section",ExpressionUUID->"6acdffb2-c662-4776-b7c6-4c7def63ee8c"],

Cell[BoxData[
 RowBox[{"(*", 
  RowBox[{
   RowBox[{"exportMultiplotsVsMuAndLearningRate", "[", 
    RowBox[{"\"\<*_joined\\\\\>\"", ",", " ", 
     RowBox[{"Range", "[", 
      RowBox[{"1", ",", "5"}], "]"}], ",", " ", 
     "\"\<_2_smallErrorVsParameters.png\>\"", ",", " ", "getErrors", ",", " ", 
     RowBox[{"{", 
      RowBox[{"0.5", ",", " ", "3", ",", " ", "0.1"}], "}"}]}], "]"}], ";"}], 
  "*)"}]], "Input",ExpressionUUID->"20de7819-68d6-45f1-b291-aaf8daf5a30d"],

Cell[BoxData[
 RowBox[{"(*", 
  RowBox[{
   RowBox[{"exportMultiplotsVsMuAndLearningRate", "[", 
    RowBox[{"\"\<*_GE*joined\\\\\>\"", ",", " ", 
     RowBox[{"Range", "[", 
      RowBox[{"1", ",", "5"}], "]"}], ",", " ", 
     "\"\<_3_smallerErrorVsParameters.png\>\"", ",", " ", "getErrors", ",", 
     " ", 
     RowBox[{"{", 
      RowBox[{"0.5", ",", " ", "3", ",", " ", "0.05"}], "}"}]}], "]"}], ";"}],
   "*)"}]], "Input",ExpressionUUID->"1e1992b5-c72b-42e7-93d5-13e1a7ca2075"],

Cell[BoxData[
 RowBox[{"(*", 
  RowBox[{
   RowBox[{"exportMultiplotsVsMuAndLearningRate", "[", 
    RowBox[{"\"\<*_GM*joined\\\\\>\"", ",", " ", 
     RowBox[{"Range", "[", 
      RowBox[{"1", ",", "5"}], "]"}], ",", " ", 
     "\"\<_3_smallerErrorVsParameters.png\>\"", ",", " ", "getErrors", ",", 
     " ", 
     RowBox[{"{", 
      RowBox[{"1.5", ",", " ", "12", ",", " ", "0.25"}], "}"}]}], "]"}], 
   ";"}], "*)"}]], "Input",ExpressionUUID->"957342e3-6692-4474-9364-\
244f5260e830"],

Cell[BoxData[
 RowBox[{"(*", 
  RowBox[{
   RowBox[{"tempSplitted", " ", ":=", 
    RowBox[{"readAndSplitByK", "[", 
     RowBox[{
      RowBox[{
       RowBox[{
       "dirs", "[", 
        "\"\<18-10-17-16-48ParameterTester_GMp_smallerEpsilon\>\"", "]"}], 
       "[", 
       RowBox[{"[", "1", "]"}], "]"}], ",", 
      RowBox[{"{", 
       RowBox[{"1", ",", "2", ",", "3", ",", " ", "4", ",", " ", "5"}], 
       "}"}]}], "]"}]}], ";"}], "*)"}]], "Input",ExpressionUUID->"060568d1-\
0f74-4996-80f3-3329da288dcf"],

Cell[BoxData[
 RowBox[{"(*", 
  RowBox[{
   RowBox[{"tempSplitted", " ", ":=", 
    RowBox[{"readAndSplitByK", "[", 
     RowBox[{
      RowBox[{
       RowBox[{
       "dirs", "[", "\"\<18-10-20ParameterTester_GEp_joined\>\"", "]"}], "[", 
       RowBox[{"[", "1", "]"}], "]"}], ",", 
      RowBox[{"{", 
       RowBox[{"1", ",", "2", ",", "3", ",", " ", "4", ",", " ", "5"}], 
       "}"}]}], "]"}]}], ";"}], "*)"}]], "Input",ExpressionUUID->"2c4be009-\
7a9a-427b-963a-71265d2d5632"],

Cell[BoxData[
 RowBox[{"(*", 
  RowBox[{
   RowBox[{
    RowBox[{"Legended", "[", 
     RowBox[{
      RowBox[{"myContourPlot", "[", 
       RowBox[{
        RowBox[{"getPoints", "[", 
         RowBox[{"tempSplitted", "[", 
          RowBox[{"[", 
           RowBox[{"3", ",", "3"}], "]"}], "]"}], "]"}], ",", 
        RowBox[{"getErrors", "[", 
         RowBox[{"tempSplitted", "[", 
          RowBox[{"[", 
           RowBox[{"3", ",", "3"}], "]"}], "]"}], "]"}], ",", 
        "errorMinMaxDif"}], "]"}], ",", 
      RowBox[{"legend", "@@", "errorMinMaxDif"}]}], "]"}], ";"}], 
   "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"Legended", "[", 
     RowBox[{
      RowBox[{"myScaledContourPlot", "[", 
       RowBox[{
        RowBox[{"getPoints", "[", 
         RowBox[{"tempSplitted", "[", 
          RowBox[{"[", 
           RowBox[{"3", ",", "3"}], "]"}], "]"}], "]"}], ",", 
        RowBox[{"getErrors", "[", 
         RowBox[{"tempSplitted", "[", 
          RowBox[{"[", 
           RowBox[{"3", ",", "3"}], "]"}], "]"}], "]"}], ",", 
        "errorMinMaxDif"}], "]"}], ",", 
      RowBox[{"legend", "@@", "errorMinMaxDif"}]}], "]"}], ";"}]}], 
  "*)"}]], "Input",ExpressionUUID->"bad43451-314b-4143-866f-808886879773"]
}, Open  ]],

Cell[CellGroupData[{

Cell["Wykresy \:015brednie", "Section",
 FormatType->
  "TextForm",ExpressionUUID->"b1f6188b-4122-4483-9e3c-0fe4e433ec8b"],

Cell[BoxData[
 RowBox[{
  RowBox[{"Export", "[", 
   RowBox[{
    RowBox[{
     RowBox[{"NotebookDirectory", "[", "]"}], "<>", 
     "\"\<\\\\meanErrorVsParameters.pdf\>\""}], ",", 
    RowBox[{"meanVsMuAndLearningRatePlot", "[", 
     RowBox[{
      RowBox[{"dirs", "[", "\"\<*joined\>\"", "]"}], ",", " ", 
      RowBox[{"Range", "[", 
       RowBox[{"1", ",", "5"}], "]"}], ",", " ", "getErrors", ",", " ", 
      RowBox[{"{", 
       RowBox[{"2", ",", "15", ",", "0.5"}], "}"}]}], "]"}]}], "]"}], 
  ";"}]], "Input",ExpressionUUID->"c4571022-6837-4d3f-b015-1884d02d4a9f"],

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{
    RowBox[{"Export", "[", 
     RowBox[{
      RowBox[{
       RowBox[{"NotebookDirectory", "[", "]"}], "<>", "\"\<\\\\G\>\"", "<>", 
       RowBox[{"ToString", "[", 
        RowBox[{"typeFromDir", "[", "#", "]"}], "]"}], "<>", 
       "\"\<MeanErrorVsParameters.pdf\>\""}], ",", 
      RowBox[{"meanVsMuAndLearningRatePlot", "[", 
       RowBox[{
        RowBox[{"{", "#", "}"}], ",", " ", 
        RowBox[{"Range", "[", 
         RowBox[{"1", ",", "5"}], "]"}], ",", " ", "getErrors", ",", " ", 
        RowBox[{"{", 
         RowBox[{"2", ",", "15", ",", "0.5"}], "}"}]}], "]"}]}], "]"}], "&"}],
    "/@", 
   RowBox[{"dirs", "[", "\"\<*joined\>\"", "]"}]}], ";"}]], "Input",Expression\
UUID->"bb65adcb-0df0-4fa9-a2e3-96ea9ddcc019"],

Cell[BoxData[
 RowBox[{
  RowBox[{"Export", "[", 
   RowBox[{
    RowBox[{
     RowBox[{"NotebookDirectory", "[", "]"}], "<>", 
     "\"\<\\\\meanErrorVsParameters_presentation.pdf\>\""}], ",", 
    RowBox[{"Show", "[", 
     RowBox[{
      RowBox[{"meanVsMuAndLearningRatePlot", "[", 
       RowBox[{
        RowBox[{"dirs", "[", "\"\<*joined\>\"", "]"}], ",", " ", 
        RowBox[{"Range", "[", 
         RowBox[{"1", ",", "5"}], "]"}], ",", " ", "getErrors", ",", " ", 
        RowBox[{"{", 
         RowBox[{"2", ",", "15", ",", "0.5"}], "}"}]}], "]"}], ",", " ", 
      RowBox[{"ListPlot", "[", 
       RowBox[{
        RowBox[{"{", 
         RowBox[{"{", 
          RowBox[{"0.8507462686567163`", ",", "2.56`*^-7"}], "}"}], "}"}], 
        ",", " ", 
        RowBox[{"ScalingFunctions", "\[Rule]", 
         RowBox[{"{", 
          RowBox[{"None", ",", " ", "\"\<Log\>\""}], "}"}]}], ",", " ", 
        RowBox[{"PlotMarkers", "\[Rule]", 
         RowBox[{"{", 
          RowBox[{
           RowBox[{"Graphics", "[", 
            RowBox[{"{", 
             RowBox[{"Red", ",", 
              RowBox[{"Disk", "[", "]"}]}], "}"}], "]"}], ",", "0.02"}], 
          "}"}]}]}], "]"}]}], "]"}]}], "]"}], ";"}]], "Input",ExpressionUUID->\
"5d52ea01-6d13-4275-b192-98538ed42181"]
}, Open  ]]
},
WindowSize->{1920, 997},
WindowMargins->{{-8, Automatic}, {Automatic, 0}},
TrackCellChangeTimes->False,
FrontEndVersion->"11.3 for Microsoft Windows (64-bit) (March 6, 2018)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[558, 20, 379, 8, 192, "Input",ExpressionUUID->"125816c6-4fcd-4623-b536-a59814d4ed04",
 InitializationCell->True],
Cell[CellGroupData[{
Cell[962, 32, 102, 0, 67, "Section",ExpressionUUID->"551cbcb1-acd1-4b06-80b9-66d3e783a7ee"],
Cell[1067, 34, 314, 7, 28, "Input",ExpressionUUID->"2190ceb4-9240-44f3-9460-db43e6b507dd"],
Cell[1384, 43, 569, 13, 48, "Input",ExpressionUUID->"e5601bcb-13f0-4ba8-b0a7-190408d386bc"],
Cell[1956, 58, 276, 6, 45, "Input",ExpressionUUID->"ddfc6f92-bc2b-4c77-8e52-b7c2c962de63"],
Cell[2235, 66, 305, 7, 28, "Input",ExpressionUUID->"16ec9326-c9d8-4025-ae14-37cd5e595506"],
Cell[2543, 75, 1593, 39, 86, "Input",ExpressionUUID->"e260cdb1-ba55-47db-b62d-bd07fad55aa2"]
}, Open  ]],
Cell[CellGroupData[{
Cell[4173, 119, 115, 0, 67, "Section",ExpressionUUID->"6acdffb2-c662-4776-b7c6-4c7def63ee8c"],
Cell[4291, 121, 473, 10, 28, "Input",ExpressionUUID->"20de7819-68d6-45f1-b291-aaf8daf5a30d"],
Cell[4767, 133, 485, 11, 28, "Input",ExpressionUUID->"1e1992b5-c72b-42e7-93d5-13e1a7ca2075"],
Cell[5255, 146, 489, 12, 28, "Input",ExpressionUUID->"957342e3-6692-4474-9364-244f5260e830"],
Cell[5747, 160, 516, 15, 28, "Input",ExpressionUUID->"060568d1-0f74-4996-80f3-3329da288dcf"],
Cell[6266, 177, 485, 13, 28, "Input",ExpressionUUID->"2c4be009-7a9a-427b-963a-71265d2d5632"],
Cell[6754, 192, 1228, 34, 48, "Input",ExpressionUUID->"bad43451-314b-4143-866f-808886879773"]
}, Open  ]],
Cell[CellGroupData[{
Cell[8019, 231, 122, 2, 67, "Section",ExpressionUUID->"b1f6188b-4122-4483-9e3c-0fe4e433ec8b"],
Cell[8144, 235, 575, 14, 45, "Input",ExpressionUUID->"c4571022-6837-4d3f-b015-1884d02d4a9f"],
Cell[8722, 251, 783, 20, 45, "Input",ExpressionUUID->"bb65adcb-0df0-4fa9-a2e3-96ea9ddcc019"],
Cell[9508, 273, 1277, 33, 139, "Input",ExpressionUUID->"5d52ea01-6d13-4275-b192-98538ed42181"]
}, Open  ]]
}
]
*)

