(* ::Package:: *)

BeginPackage[ "PlotGrid`"]

  PlotGrid::usage = 
	"PlotGrid[l_List,singleW_,singleH_,opts:OptionsPattern[{plotGrid,Graphics}]] draws a grid l of plots, each resized to {singleW,silngleH} size.
Additional Graphics options can be passed to resulting graphic, except of ImagePadding, which is used internally."

Begin[ "Private`"]

Protect[imageMargin]
Options[PlotGrid]={ImagePadding->{{15,5},{15,5}}, imageMargin -> 10};
PlotGrid[l_List,singleW_,singleH_,opts:OptionsPattern[{PlotGrid,Graphics}]]:=
	Module[{
		nx,
		ny,
		w,
		h,
		sidePadding=OptionValue[PlotGrid,ImagePadding],
		imageMargin=OptionValue[imageMargin],
		widths,
		heights,
		dimensions,
		positions,
		frameOptions=FilterRules[{opts},FilterRules[Options[Graphics],Except[{ImagePadding}]]]
	},
	{ny,nx}=Dimensions[l];
	w=singleW*nx;
	h=singleH*ny;
	widths=(w-Plus@@sidePadding[[1]]  + imageMargin)/nx Table[1,{nx}];
	widths[[1]]=widths[[1]]+sidePadding[[1]][[1]]- imageMargin/2;
	widths[[-1]]=widths[[-1]]+sidePadding[[1]][[2]]- imageMargin/2;
	heights=(h-Plus@@sidePadding[[2]] + imageMargin)/ny Table[1,{ny}];
	heights[[1]]=heights[[1]]+sidePadding[[2]][[1]]- imageMargin/2;
	heights[[-1]]=heights[[-1]]+sidePadding[[2]][[2]]- imageMargin/2;
	positions=Transpose@Partition[Tuples[Prepend[Accumulate[Most[#]],0]&/@{widths,heights}],ny];
	Graphics[
		Table[Inset[Show[l[[ny-j+1,i]],
			ImagePadding->{
				{If[i==1,sidePadding[[1]][[1]],imageMargin/2], If[i==nx,sidePadding[[1]][[2]], imageMargin/2]},
				{If[j==1,sidePadding[[2]][[1]],imageMargin/2], If[j==ny,sidePadding[[2]][[2]], imageMargin/2] }
			},
			FrameTicksStyle->{If[i==1,Automatic,Directive[FontOpacity->0, FontSize->0]],If[j==1,Automatic,Directive[FontOpacity->0, FontSize->0]]},
			FrameStyle->{
				{If[i==1,Automatic,Directive[FontOpacity->0, FontSize->0]],Automatic},
				{Automatic,If[j==ny,Automatic,Directive[FontOpacity->0, FontSize->0]]}
			},
			AxesStyle->{If[j==1,Automatic,Directive[FontOpacity->0, FontSize->0]],If[i==1,Automatic,Directive[FontOpacity->0, FontSize->0]]},
			AspectRatio->Full],positions[[j,i]],{Left,Bottom},{widths[[i]],heights[[j]]}],{i,1,nx},{j,1,ny}
		],
		PlotRange->{{0,w},{0,h}},
		ImageSize->{w,h},
		Evaluate@Apply[Sequence,frameOptions]]
	]

End[]

EndPackage[]
